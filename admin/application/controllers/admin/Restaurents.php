<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Restaurents extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
    }


    public function index()
    {
        _check_user_login();
        $type = $this->input->get("type");
        if ($type == "edit") {
            $resId = $this->input->get("id");
            $category = $this->getRestaurents($resId);
            $data['data'] = $category ? $category[0] : array();
        }

        $data['list'] = $this->getRestaurents();
        $data['cities'] = $this->getCities();
        $data['resto'] = $this->getRestorentCat();


        $data['type'] = $type;
        $data['template'] = 'admin/dashboard/restaurents';
        $this->load->view('admin/template/dashboard/layout', $data);
    }

    private function getRestaurents($resId = null)
    {
        $conditions = array();
        if ($resId) {
            $conditions['r.restaurent_id'] = $resId;
        }
        $joins = array(
            array("city_list as cl", "cl.city_id=r.restaurent_city_id", "left")
        );
        $columns = array(
            "r.*",
            "cl.city_name",
        );
        /* $columns = array(
             "r.*",
         );*/

        $result = $this->query_model->getRows("restaurents as r", $conditions, $columns, null, $joins);
      $resList = array();
      foreach($result as $res) {
        $resObj = $res;
        $condition = array("rci.restaurant_id" => $res->restaurent_id);
        $columns = array(
          "rci.category_id",
          "cr.category_name"
        );
        $joins = array(
        array("category_restaurant as cr", "cr.category_id=rci.category_id", "inner")
        );
        $catList = $this->query_model->getRows("restaurants_cat_id as rci", $condition, $columns, null, $joins);
        $resObj->categories = $catList?$catList:array();
        $resList[] = $resObj;
      }
        return $resList ? $resList : array();
    }

    private function getCities($resId = null)
    {
        $conditions = array();
        if ($resId) {
            $conditions['c.city_id'] = $resId;
        }

        $columns = array(
            "r.*",
        );

        $result = $this->query_model->getRows("city_list as r", $conditions, $columns);
        return $result ? $result : array();
    }

    private function getRestorentCat($resId = null)
    {
        $conditions = array();
        if ($resId) {
            $conditions['c.category_id_id'] = $resId;
        }

        $columns = array(
            "r.*",
        );

        $result = $this->query_model->getRows("category_restaurant as r", $conditions, $columns);
        return $result ? $result : array();
    }

    public function deleteRestaurent()
    {
        $resId = $this->input->get("id");
        $condition = array(
            "restaurent_id" => $resId,
        );

        $result = $this->query_model->deleteRow("restaurents", $condition);
        $this->query_model->deleteRow("restaurants_cat_id", array("restaurant_id"=>$resId));
        redirect("admin/restaurents");
    }

    public function addRestaurent()
    {

      $fileName = "";
      $path="";
      if(!empty($_FILES['photo']['name'])) {
         $path = '/assets/uploads/partners/';
        $dir = '.' . $path;

        if (!is_dir($dir)) {
            mkdir($dir, 0777, TRUE);
        }

        $param = array(
            'file_name' => 'photo',
            'upload_path' => $dir,
            'allowed_types' => '*',
            'image_resize' => FALSE,
            'source_image' => $dir,
            'new_image' => $dir,
            'encrypt_name' => TRUE,
        );
        $upload_file = upload_file($param);

        $fileName = $upload_file['UPLOAD_DATA']['file_name'];
      }



        //  $insertRsult = $this->query_model->updateRow("restaurent", array("restaurent_id" => $restaurentId), $data);

        /* if ($upload_file['STATUS'] && $insertRsult) {
             $this->response([
                 'status' => true,
                 'message' => "Photo updated successfully",
                 'data' => $this->query_model->getRow("restaurent", array("restaurent_id" => $restaurentId))
             ], 200);
         } else {
             $this->response([
                 'status' => false,
                 'message' => $upload_file['FILE_ERROR']
             ], 200);
         }*/

        $catName = $this->input->post('restaurentName');
        $restaurentPartnerName = $this->input->post('restaurentPartnerName');
        $restaurentCityId = $this->input->post('restaurentCityId');
        $restaurentPhone = $this->input->post('restaurentPhone');
        $restaurentLat = $this->input->post('restaurentLat');
        $restaurentLng = $this->input->post('restaurentLng');
        $restaurentAddress = $this->input->post('restaurentAddress');
        $restaurentCategoryId = $this->input->post('restaurentCategoryId');
        $restaurentStatus = $this->input->post('restaurentStatus');

        $resId = $this->input->post('restaurent_id');

        $message = "";
        $status = false;
        $resData = null;
        $conditionCat = array(
            "restaurent_phone" => $restaurentPhone,
        );

        if ($resId) {
            $conditionCat['restaurent_id !='] = $resId;
        }

        $checkAlready = $this->query_model->getRow("restaurents", $conditionCat);
        if ($checkAlready) {
            $message = "res already exists";
        } else {

$uniqueCode = $this->generateUniqueString(8);
            $data = array(
                "restaurent_name" => $catName,
                "restaurent_partner_name" => $restaurentPartnerName,
                "restaurent_city_id	" => $restaurentCityId,
                "restaurent_phone" => $restaurentPhone,
                "restaurent_latitude" => $restaurentLat,
                "restaurent_longitude" => $restaurentLng,
                "restaurent_address" => $restaurentAddress,
                "restaurent_status" => $restaurentStatus,
"restaurent_code" => $uniqueCode
            );
          if(!empty($fileName)) {
            $data["restaurent_partner_photo"] = $path . "" . $fileName;
          }
            if ($resId) {
                $resultId = $this->query_model->updateRow("restaurents", array("restaurent_id" => $resId), $data);
                $resultId = $resId;
            } else {
                $resultId = $this->query_model->insertRow("restaurents", $data);
            }
            if ($resultId) {
                 $this->query_model->deleteRow("restaurants_cat_id", array("restaurant_id"=>$resultId));

                foreach ($restaurentCategoryId as $catId) {
                    $data = array(
                        "category_id" => $catId,
                        "restaurant_id" => $resultId,
                    );
                  $conditions = array(
                    "restaurant_id" => $resultId,
                    "category_id" => $catId
                  );
                  $checkAlready = $this->query_model->getRow("restaurants_cat_id",$conditions);
                  if(!$checkAlready) {
                    $this->query_model->insertRow("restaurants_cat_id", $data);
                  }
                }
                $status = true;
                $message = "Successfully added Restaurent";
            } else {
                $message = "Restaurent not added";
            }
        }


        redirect("admin/restaurents");
    }

private function generateUniqueString($length = 8)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return strtoupper(implode($pass)); //turn the array into a string
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
