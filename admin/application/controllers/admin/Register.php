<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->model('email_model');
        $this->load->helper('URL');
    }

    public function index()
    {
        $data['template'] = 'admin/auth/register';
        $this->load->view('admin/template/auth/layout', $data);
    }


    public function signup()
    {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($fname)) {
            $message = "fname is required";
        } else if (!isset($lname)) {
            $message = "lname is required";
        } else if (!isset($mobile)) {
            $message = "mobile is required";
        } else if (!isset($email)) {
            $message = "email is required";
        } else if (!isset($password)) {
            $message = "password is required";
        } else {
            $result = $this->query_model->getRow("user", array("user_mail" => $email));
            if ($result) {
                $message = "Email already used";
            } else {
                $data = array(
                    'user_first' => $fname,
                    'user_last' => $lname,
                    'user_mail' => $email,
                    'user_password' => $password,
                    'user_phone' => $mobile
                );
                $result = $this->query_model->insertRow("user", $data);
                if ($result) {
                    $resData = $this->query_model->getRow("user", array("user_mail" => $email));
                    $status = true;
                    $message = "Signup successfully";
                } else {
                    $message = "Something went wrong";
                }
            }
        }

        if($status) {
            $this->session->set_userdata('user_data',$resData);
            redirect("admin/users");
        } else {
            redirect("register");
        }
    }


    public function signupRequest()
    {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($fname)) {
            $message = "fname is required";
        } else if (!isset($lname)) {
            $message = "lname is required";
        } else if (!isset($mobile)) {
            $message = "mobile is required";
        } else if (!isset($email)) {
            $message = "email is required";
        } else if (!isset($password)) {
            $message = "password is required";
        } else {
            $result = $this->query_model->getRow("user", array("user_mail" => $email));
            if ($result) {
                $message = "Email already used";
            } else {
                $data = array(
                    'user_first' => $fname,
                    'user_last' => $lname,
                    'user_mail' => $email,
                    'user_password' => $password,
                    'user_phone' => $mobile
                );
                $result = $this->query_model->insertRow("user", $data);
                if ($result) {
                    $resData = $this->query_model->getRow("user", array("user_mail" => $email));
                    $status = true;
                    $message = "Signup successfully";
                } else {
                    $message = "Something went wrong";
                }
            }
        }

        $response = array('data' => $resData,
            'status' => $status,
            'message' => $message);
        echo json_encode($response);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */