<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ForgotPassword extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->model('email_model');
        $this->load->helper('URL');
    }

    public function index()
    {
        $data['template'] = 'admin/auth/forgot-password';
        $this->load->view('admin/template/auth/layout', $data);
    }

    public function reset()
    {

        $email = $this->input->post('email');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($email)) {
            $message = "email is required";
        } else {
            $result = $this->query_model->getRow("user", array("user_mail" => $email));
            if (!$result) {
                $message = "Email not found";
            } else {
                $resetCode = mt_rand(1111111111111111, 9999999999999999) . mt_rand(1111111111111111, 9999999999999999);
                $result = $this->query_model->updateRow("user", array("user_mail" => $email), array("reset_password_code" => $resetCode));
                $message = "Click on this link to set new password <a href='" . base_url() . "resetPassword?resetid=" . $resetCode . "'>Reset Password</a>";

                if ($this->email_model->sendEmail($email, 'Reset Password', $message)) {
                    $status = true;
                    $message = "Reset password link sent successfully";
                } else {
                    $message = "Something went wrong";
                }
            }
        }

        if($status) {
            redirect("signin");
        } else {
            redirect("forgotpassword");
        }
    }

    public function resetRequest()
    {

        $email = $this->input->post('email');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($email)) {
            $message = "email is required";
        } else {
            $result = $this->query_model->getRow("user", array("user_mail" => $email));
            if (!$result) {
                $message = "Email not found";
            } else {
                $resetCode = mt_rand(1111111111111111, 9999999999999999) . mt_rand(1111111111111111, 9999999999999999);
                $result = $this->query_model->updateRow("user", array("user_mail" => $email), array("reset_password_code" => $resetCode));
                $message = "Click on this link to set new password <a href='" . base_url() . "resetPassword?resetid=" . $resetCode . "'>Reset Password</a>";

                if ($this->email_model->sendEmail($email, 'Reset Password', $message)) {
                    $status = true;
                    $message = "Reset password link sent successfully";
                } else {
                    $message = "Something went wrong";
                }
            }
        }

        $response = array('data' => $resData,
            'status' => $status,
            'message' => $message);
        echo json_encode($response);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */