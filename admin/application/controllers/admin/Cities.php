<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cities extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
    }


    public function index()
    {
        _check_user_login();
        $type = $this->input->get("type");
        if ($type == "edit") {
            $cityId = $this->input->get("id");
            $city = $this->getCities($cityId);
            $data['data'] = $city ? $city[0] : array();
        }

        $data['list'] = $this->getCities();
        $data['type'] = $type;
        $data['template'] = 'admin/dashboard/cities';
        $this->load->view('admin/template/dashboard/layout', $data);
    }

    private function getCities($cityId = null)
    {
        $conditions = array();
        if ($cityId) {
            $conditions['cr.city_id'] = $cityId;
        }

        $columns = array(
            "cr.*",
        );

        $result = $this->query_model->getRows("city_list as cr", $conditions, $columns);
        return $result ? $result : array();
    }

    public function deleteCity()
    {
        $cityId = $this->input->get("id");
        $condition = array(
            "city_id" => $cityId,
        );
        $result = $this->query_model->deleteRow("city_list", $condition);
        redirect("admin/cities");
    }

    public function addCity()
    {
        $catName = $this->input->post('cityName');

        $catId = $this->input->post('cityId');

        $message = "";
        $status = false;
        $resData = null;


        $conditionCat = array(
            "city_name" => $catName,
        );

        if ($catId) {
            $conditionMail['city_id !='] = $catId;
        }

        $checkAlready = $this->query_model->getRow("city_list", $conditionCat);
        if ($checkAlready) {
            $message = "City already exists";
        } else {
            $data = array(
                "city_name" => $catName,
            );
            if ($catId) {
                $resultId = $this->query_model->updateRow("city_list", array("city_id" => $catId), $data);
                $resultId = $catId;
            } else {
                $resultId = $this->query_model->insertRow("city_list", $data);
            }
            if ($resultId) {
                $status = true;
                $message = "Successfully added";
            } else {
                $message = "Category not added";
            }
        }

        redirect("admin/cities");
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */