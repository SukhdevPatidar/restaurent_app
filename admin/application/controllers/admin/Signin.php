<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->model('email_model');
        $this->load->helper('URL');
    }

    public function index()
    {
        _check_auto_login();
        $data['template'] = 'admin/auth/login';
        $this->load->view('admin/template/auth/layout', $data);
    }

    public function login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');


        $status = false;
        $message = "";
        $resData = null;

        if (empty($email)) {
            $message = "email is required";
        } else if (empty($password)) {
            $message = "password is required";
        } else {
            $data = array(
                'user_mail' => $email,
                'user_role' => array(1, 3),
            );

            $result = $this->query_model->getRow("user", $data);
            if ($result) {
                $data["user_password"] = md5($password);
                $result = $this->query_model->getRow("user", $data);
                if ($result) {
                    $status = true;
                    $message = "Login success";
                    $resData = $result;
                } else {
                    $message = "Email or password not match";
                }
            } else {
                $message = "User not found";
            }
        }

        if ($status) {
            $this->session->set_userdata('user_data', $resData);
            redirect("admin/dashboard");
        } else {
            redirect("signin");
        }
    }


    public function loginRequest()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($email)) {
            $message = "email is required";
        } else if (empty($password)) {
            $message = "password is required";
        } else {
            $data = array(
                'user_mail' => $email
            );

            $result = $this->query_model->getRow("user", $data);
            if ($result) {
                $data["user_password"] = $password;
                $result = $this->query_model->getRow("user", $data);
                if ($result) {
                    $status = true;
                    $message = "Login success";
                    $resData = $result;
                } else {
                    $message = "Email or password not match";
                }
            } else {
                $message = "User not found";
            }
        }

        $response = array('data' => $resData,
            'status' => $status,
            'message' => $message);
        echo json_encode($response);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */