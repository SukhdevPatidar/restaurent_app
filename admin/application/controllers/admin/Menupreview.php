<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menupreview extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
    }


    public function index()
    {
        _check_user_login();
        $menuId = $this->input->get("menu_id");
        $resId = $this->input->get("res_id");

        $type = $this->input->get("type");
        if ($type == "edit") {
            $catId = $this->input->get("id");
            $category = $this->getCategories($menuId, $catId);
            $data['data'] = $category ? $category[0] : array();
        }

        $data['list'] = $this->getCategories($resId, $menuId);
        $data['menu_id'] = $menuId;
        $data['res_id'] = $resId;

        $data['type'] = $type;
        $data['template'] = 'admin/dashboard/menupreview';
        $this->load->view('admin/template/dashboard/layout', $data);
    }

    private function getCategories($resId, $menuId, $itemId = null)
    {
        $itemConditions = array();
        if ($resId) {
            $itemConditions['cr.restaurent_id'] = $resId;
        }

        $columns = array(
            "cr.*",
        );
        $orderBy = array(
            array("category_order", "ASC")
        );

        $result = $this->query_model->getRows("category_menu as cr", $itemConditions, $columns, $orderBy);

        foreach ($result as $data) {
            $itemConditions = array(
                'mr.item_category_id' => $data->category_id
            );

            $joins = array(
                array("category_menu as cr", "cr.category_id=mr.item_category_id", "inner")
            );
            $itemsColumns = array(
                "mr.*",
                "cr.category_name",
            );

            $items = $this->query_model->getRows("menu_items as mr", $itemConditions, $itemsColumns, null, $joins);

            $data->items = $items ? $items : array();
        }


        return $result ? $result : array();
    }

    public function deleteCategory()
    {
        $resId = $this->input->get("res_id");
        $catId = $this->input->get("menu_id");
        $itemId = $this->input->get("id");
        $condition = array(
            "item_id" => $itemId,
        );
        $result = $this->query_model->deleteRow("menu_items", $condition);
        redirect("admin/menupreview?menu_id=" . $catId . "&res_id=" . $resId);
    }

    /**
     *
     */
    public function addItems()

    {
        $fileName = "";
        $path = "";
        if (!empty($_FILES['photo']['name'])) {
            $path = '/assets/uploads/items/';
            $dir = '.' . $path;

            if (!is_dir($dir)) {
                mkdir($dir, 0777, TRUE);
            }

            $param = array(
                'file_name' => 'photo',
                'upload_path' => $dir,
                'allowed_types' => '*',
                'image_resize' => FALSE,
                'source_image' => $dir,
                'new_image' => $dir,
                'encrypt_name' => TRUE,
            );
            $upload_file = upload_file($param);

            $fileName = $upload_file['UPLOAD_DATA']['file_name'];
        }

        $menuId = $this->input->post('menuId');
        $resId = $this->input->post("resId");
        $itemId = $this->input->post("itemId");


        $itemName = $this->input->post('itemName');
        $itemPrice = $this->input->post('itemPrice');
        $itemDescription = $this->input->post('itemDescription');


        $message = "";
        $status = false;
        $resData = null;


        $conditionCat = array(
            "item_name" => $itemName,
            "item_restaurent_id" => $resId,
        );

        if ($itemId) {
            $conditionCat['item_id !='] = $itemId;
        }

        $checkAlready = $this->query_model->getRow("menu_items", $conditionCat);

        if ($checkAlready) {
            $message = " items already exists";
        } else {
            $data = array(
                "item_name" => $itemName,
                "item_category_id" => $menuId,
                "item_price" => $itemPrice,
                "item_description" => $itemDescription,
                "item_restaurent_id" => $resId
            );
            if (!empty($fileName)) {
                $data["item_photo"] = $path . "" . $fileName;
            }

            if ($itemId) {
                $resultId = $this->query_model->updateRow("menu_items", array("item_id" => $itemId), $data);
                $resultId = $itemId;
            } else {
                $resultId = $this->query_model->insertRow("menu_items", $data);
            }
            if ($resultId) {
                $status = true;
                $message = "Successfully added";
            } else {
                $message = "Category not added";
            }
        }

        redirect("admin/menupreview?menu_id=" . $menuId . "&res_id=" . $resId);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */