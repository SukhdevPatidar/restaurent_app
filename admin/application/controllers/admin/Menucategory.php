<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menucategory extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
    }


    public function index()
    {
        _check_user_login();
        $resId = $this->input->get("res_id");
        $type = $this->input->get("type");
        if ($type == "edit") {
            $catId = $this->input->get("id");
            $category = $this->getCategories($resId, $catId);
            $data['data'] = $category ? $category[0] : array();
        }

        $data['list'] = $this->getCategories($resId);
        $data['res_id'] = $resId;
        $data['type'] = $type;
        $data['template'] = 'admin/dashboard/menucategory';
        $this->load->view('admin/template/dashboard/layout', $data);
    }

    private function getCategories($resId, $catId = null)
    {
        $conditions = array();
        if (!$resId) {
            return array();
        }
        $conditions['cr.restaurent_id'] = $resId;

        if ($catId) {
            $conditions['cr.category_id'] = $catId;
        }

        $columns = array(
            "cr.*",
        );
        $orderBy = array(
            array("category_order", "ASC")
        );

        $result = $this->query_model->getRows("category_menu as cr", $conditions, $columns, $orderBy);
        foreach ($result as $data) {
            $count = $this->query_model->getRows("menu_items as mi", array("mi.item_category_id" => $data->category_id));
            $data->items_count = $count ? count($count) : 0;
        }
        return $result ? $result : array();
    }

    public function updateOrder()
    {

        $categories = $this->input->post("categories");
        $resId = $this->input->post("resId");

        foreach ($categories as $key => $value) {
            $this->query_model->updateRow("category_menu", array("category_id" => $value, "restaurent_id" => $resId), array("category_order" => $key));
        }

        $newRes = array(
            "success" => true,
            "categories" => $categories,
        );
        echo json_encode($newRes);
    }

    public function deleteCategory()
    {
        $resId = $this->input->get("res_id");
        $catId = $this->input->get("id");
        $condition = array(
            "category_id" => $catId,
        );
        $result = $this->query_model->deleteRow("category_menu", $condition);
        redirect("admin/menucategory?res_id=" . $resId);
    }

    public function addCategory()
    {
        $catName = $this->input->post('categoryName');

        $catId = $this->input->post('catId');
        $resId = $this->input->post('res_id');

        $message = "";
        $status = false;
        $resData = null;


        $conditionCat = array(
            "category_name" => $catName,
            "restaurent_id" => $resId,
        );

        if ($catId) {
            $conditionMail['category_id !='] = $catId;
        }

        $checkAlready = $this->query_model->getRow("category_menu", $conditionCat);
        if ($checkAlready) {
            $message = "Category already exists";
        } else {
            $data = array(
                "category_name" => $catName,
                "restaurent_id" => $resId,
            );
            if ($catId) {
                $resultId = $this->query_model->updateRow("category_menu", array("category_id" => $catId), $data);
                $resultId = $catId;
            } else {
                $resultId = $this->query_model->insertRow("category_menu", $data);
            }
            if ($resultId) {
                $status = true;
                $message = "Successfully added";
            } else {
                $message = "Category not added";
            }
        }

        redirect("admin/menucategory?res_id=" . $resId);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */