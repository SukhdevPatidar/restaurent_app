<?php if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class Users extends CI_Controller
{

  /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  *        http://example.com/index.php/welcome
  *    - or -
  *        http://example.com/index.php/welcome/index
  *    - or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see http://codeigniter.com/user_guide/general/urls.html
  */

  public function __construct()
  {
    parent::__construct();
    $this->load->model('query_model');
    $this->load->helper('URL');
  }


  public function index()
  {
    _check_user_login();
    $type = $this->input->get("type");
    if ($type == "edit") {
      $userId = $this->input->get("id");
      $users = $this->getUsers($userId);
      $user = $users ? $users[0] : array();
      $user->userHomeAddress = $this->getUserAddress($userId, 'H');
      $user->userOfficeAddress = $this->getUserAddress($userId, 'O');
      $data['data'] = $user;
    }

    $data['users'] = $this->getUsers();
    $data['cities'] = $this->getCities();

    $data['type'] = $type;
    $data['template'] = 'admin/dashboard/users';
    $this->load->view('admin/template/dashboard/layout', $data);
  }


  private function getUserAddress($userId, $addressType=null)
  {
    $condtions = array(
      "location_user_id" => $userId,
    );
    if ($addressType) {
      $condtions["location_type"] = $addressType;
    }
    $columns = array(
      "location_address",
      "location_lat",
      "location_lng",
      "location_type",
    );

    $address  = $this->query_model->getRow("user_location", $condtions, $columns);

    return $address;
  }

  private function getCities($cityId = null)
  {
    $conditions = array();
    if ($cityId) {
      $conditions['c.city_id'] = $cityId;
    }

    $columns = array(
      "r.*",
    );

    $result = $this->query_model->getRows("city_list as r", $conditions, $columns);
    return $result ? $result : array();
  }


  private function getUsers($userId = null)
  {
    $condition = array(
      "user_role" => 2
    );
    if ($userId) {
      $condition['user_id'] = $userId;
    }
$columns = array("user.*", "cl.city_name as user_city_name");
$joins = array(
array("city_list as cl", "cl.city_id=user.user_city_id", "left")
);
    $users = $this->query_model->getRows("user", $condition,$columns,NULL,$joins);
foreach($users as $resObj) {
$tz = new DateTimeZone('Europe/Rome');

$today = new DateTime($resObj->user_created);
$today->setTimezone($tz);
$resObj->user_created = $today->format('Y-m-d H:i:s');
}
    return $users;
  }


  public function deleteUser()
  {
    $catId = $this->input->get("id");
    $condition = array(
      "user_id" => $catId,
    );

    $result = $this->query_model->deleteRow("user", $condition);
    redirect("admin/users");
  }

  public function addUser()
  {

    // upload Photo
    $fileName = "";
    $path="";
    if (!empty($_FILES['photo']['name'])) {
      $path = '/assets/uploads/user/';
      $dir = '.' . $path;

      if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
      }

      $param = array(
        'file_name' => 'photo',
        'upload_path' => $dir,
        'allowed_types' => '*',
        'image_resize' => false,
        'source_image' => $dir,
        'new_image' => $dir,
        'encrypt_name' => true,
      );
      $upload_file = upload_file($param);

      $fileName = $upload_file['UPLOAD_DATA']['file_name'];
    }


    //user
    $userName = $this->input->post('userName');
    $userSurname = $this->input->post('userSurname');
    $userEmail = $this->input->post('userEmail');
    $userPhone = $this->input->post('userPhone');
    $userStatus = $this->input->post('userStatus');
    $userCityId = $this->input->post('userCityId');

    //address
    $homeAddress = $this->input->post('homeAddress');
    $homeAddressLat = $this->input->post('homeAddressLat');
    $homeAddressLng = $this->input->post('homeAddressLng');
    $officeAddress = $this->input->post('officeAddress');
    $officeAddressLat = $this->input->post('officeAddressLat');
    $officeAddressLng = $this->input->post('officeAddressLng');

    $userId = $this->input->post('user_id');

    $message = "";
    $status = false;
    $resData = null;
    $conditionUser = array(
      "user_phone" => $userPhone,
    );

    if ($userId) {
      $conditionUser['user_id !='] = $userId;
    }
    
    $checkAlready = $this->query_model->getRow("user", $conditionUser);
    if ($checkAlready) {
      $message = "This mobile number linked with another account";
    } else {
      $data = array(
        "user_first" => $userName,
        "user_last" => $userSurname,
        "user_mail" => $userEmail,
        "user_phone	" => $userPhone,
        "user_status" => $userStatus,
        "user_city_id" => $userCityId,
        "user_role" => 2,
      );
      if(!empty($fileName)) {
        $data["user_photo"] = $path . "" . $fileName;
      }

      if ($userId) {
        $this->query_model->updateRow("user", array("user_id" => $userId), $data);
        $resultId = $userId;
      } else {
        $resultId = $this->query_model->insertRow("user", $data);
      }
      if ($resultId) {
        //update user address
        if (!empty($homeAddress)) {
          $this->updateUserAddress($homeAddress, $homeAddressLat, $homeAddressLng, $resultId, 'H');
        }
        if (!empty($officeAddress)) {
          $this->updateUserAddress($officeAddress, $officeAddressLat, $officeAddressLng, $resultId, 'O');
        }
        $status = true;
        $message = "User successfully saved";
      } else {
        $message = "User not saved";
      }
    }


    $this->session->set_flashdata('status', $status);
    $this->session->set_flashdata('message', $message);

    redirect("admin/users");
  }

  private function updateUserAddress($userAddress, $userLat, $userLng, $userId, $addressType)
  {
    $conditions = array(
      "location_user_id" => $userId,
      "location_type" => $addressType,
    );
    $data = array(
      "location_address" => $userAddress,
      "location_lat" => $userLat,
      "location_lng" => $userLng,
      "location_user_id" => $userId,
      "location_type" => $addressType,
    );
    $checkAlready = $this->query_model->getRow("user_location", $conditions);
    if ($checkAlready) {
      $this->query_model->updateRow("user_location", $conditions, $data);
    } else {
      $this->query_model->insertRow("user_location", $data);
    }
    return true;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
