<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rescategory extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
    }


    public function index()
    {
        _check_user_login();
        $type = $this->input->get("type");
        if ($type == "edit") {
            $catId = $this->input->get("id");
            $category = $this->getCategories($catId);
            $data['data'] = $category ? $category[0] : array();
        }

        $data['list'] = $this->getCategories();
        $data['type'] = $type;
        $data['template'] = 'admin/dashboard/rescategory';
        $this->load->view('admin/template/dashboard/layout', $data);
    }

    private function getCategories($catId = null)
    {
        $conditions = array();
        if ($catId) {
            $conditions['cr.category_id'] = $catId;
        }

        $columns = array(
            "cr.*",
        );

        $result = $this->query_model->getRows("category_restaurant as cr", $conditions, $columns);
        return $result ? $result : array();
    }

    public function deleteCategory()
    {
        $catId = $this->input->get("id");
        $condition = array(
            "category_id" => $catId,
        );
        $result = $this->query_model->deleteRow("category_restaurant", $condition);
        redirect("admin/rescategory");
    }

    public function addCategory()
    {

        // upload Photo
      $fileName = "";
      $path="";
      if(!empty($_FILES['photo']['name'])) {
         $path = '/assets/uploads/rescategory/';
        $dir = '.' . $path;

        if (!is_dir($dir)) {
            mkdir($dir, 0777, TRUE);
        }

        $param = array(
            'file_name' => 'photo',
            'upload_path' => $dir,
            'allowed_types' => '*',
            'image_resize' => FALSE,
            'source_image' => $dir,
            'new_image' => $dir,
            'encrypt_name' => TRUE,
        );
        $upload_file = upload_file($param);

        $fileName = $upload_file['UPLOAD_DATA']['file_name'];
      }


        $catName = $this->input->post('categoryName');
        $catId = $this->input->post('catId');

        $message = "";
        $status = false;
        $resData = null;


        $conditionCat = array(
            "category_name" => $catName,
        );

        if ($catId) {
            $conditionCat['category_id !='] = $catId;
        }

        $checkAlready = $this->query_model->getRow("category_restaurant", $conditionCat);
        if ($checkAlready) {
            $message = "Category already exists";
        } else {
            $data = array(
                "category_name" => $catName,
            );
            if(!empty($fileName)) {
              $data["category_icon_path"] = $path . "" . $fileName;
            }

            if ($catId) {
                $resultId = $this->query_model->updateRow("category_restaurant", array("category_id" => $catId), $data);
                $resultId = $catId;
            } else {
                $resultId = $this->query_model->insertRow("category_restaurant", $data);
            }
            if ($resultId) {
                $status = true;
                $message = "Successfully added";
            } else {
                $message = "Category not added";
            }
        }

        redirect("admin/rescategory");
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
