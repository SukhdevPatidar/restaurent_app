<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ResetPassword extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->helper('URL');
        $this->load->library('session');
    }

    public function index()
    {
        $resetId = $this->input->get('resetid');
        if ($resetId) {
            $result = $this->query_model->getRow("user", array("reset_password_code" => $resetId));
            if ($result) {
                $data = array("resetId" => $resetId);
            } else {
                $data = array("error" => "Invalid link or expired");
            }
            $data['template'] = 'admin/auth/reset-password';
            $this->load->view('admin/auth/template/layout', $data);
        }
    }

    public function updatedPassword()
    {
        $password = $this->input->post('password');
        $resetId = $this->input->post('resetId');

        $data = array();
        $result = $this->query_model->updateRow("user", array("reset_password_code" => $resetId), array("user_password" => $password, "reset_password_code" => ""));
        if ($result) {
            $data['success'] = 'Password successfully reset';
        } else {
            $data['error'] = 'Password not reset';
        }
        $data['template'] = 'admin/auth/reset-password';
        $this->load->view('admin/auth/template/layout', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */