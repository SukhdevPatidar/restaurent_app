<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Data extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
    }

    public function index_get()
    {
        $this->response(array(
            'status' => false,
            'message' => "unknown method"
        ), 200);
    }


    public function resCatList_get()
    {
        $status = false;
        $message = "";
        $resData = null;

        $cityId = $this->input->get('city_id');

        $conditions = array();
        if ($cityId) {
            $conditions['restaurents.restaurent_city_id'] = $cityId;
        }
        $columns = array(
            "cr.*",
            "COUNT(rci.restaurant_id) as restaurant_count"
        );
        $groupBy = array("cr.category_id");
        $joins = array(
            array("restaurants_cat_id as rci", "rci.category_id=cr.category_id", "left"),
            array("restaurents", "restaurents.restaurent_id=rci.restaurant_id", "left"),
        );

        $list = $this->query_model->getRows("category_restaurant as cr", $conditions, $columns, null, $joins, $groupBy);
        if ($list) {
            $status = true;
            $resData = $list;
        } else {
            $message = "No category restaurant found.";
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function resList_get()
    {
        $catId = $this->input->get('category_id');
        $cityId = $this->input->get('city_id');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($catId)) {
            $message = "category_id is required";
        } else {
            $conditions = array(
                "rci.category_id" => $catId
            );
            if ($cityId) {
                $conditions['restaurents.restaurent_city_id'] = $cityId;
            }

            $columns = array(
                "restaurents.*",
            );
            $groupBy = array();
            $joins = array(
                array("restaurents", "restaurents.restaurent_id=rci.restaurant_id", "inner")
            );
            $list = $this->query_model->getRows("restaurants_cat_id as rci", $conditions, $columns, null, $joins, $groupBy);
            if ($list) {
                $status = true;
                $resData = $list;
            } else {
                $message = "No restaurents found.";
            }
        }

        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function getCities_get()
    {
        $status = false;
        $message = "";
        $resData = null;

        $condition = array();
        $list = $this->query_model->getRows("city_list", $condition);
        if ($list) {
            $status = true;
            $resData = $list;
        } else {
            $message = "No cities found.";
        }

        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'canLoginSkip' => 0,
            'message' => $message
        ), 200);
    }

    public function resMenuList_get()
    {
        $status = false;
        $message = "";
        $resData = null;

        $restaurentId = $this->input->get('restaurent_id');

        $itemConditions = array();
        if ($restaurentId) {
            $itemConditions['cr.restaurent_id'] = $restaurentId;
        }

        $columns = array(
            "cr.*",
        );
        $orderBy = array(
            array("category_order", "ASC")
        );

        $result = $this->query_model->getRows("category_menu as cr", $itemConditions, $columns, $orderBy);

        foreach ($result as $data) {
            $itemConditions = array(
                'mr.item_category_id' => $data->category_id
            );

            $joins = array(
                array("category_menu as cr", "cr.category_id=mr.item_category_id", "inner")
            );
            $itemsColumns = array(
                "mr.*",
                "cr.category_name",
            );

            $items = $this->query_model->getRows("menu_items as mr", $itemConditions, $itemsColumns, null, $joins);

            $data->items = $items ? $items : array();
        }

        if ($result) {
            $status = true;
            $resData = $result;
        } else {
            $message = "No menu found.";
        }

        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function getResUserPositions_get()
    {
        $status = false;
        $message = "";
        $resData = null;
        $restaurentData = null;

        $restaurentCode = $this->input->get('restaurent_id');


        if (!isset($restaurentCode)) {
            $message = "restaurent_id is required";
        } else {
            $checkValidId = $this->query_model->getRow("restaurents", array("restaurent_code" => $restaurentCode));
            if ($checkValidId) {
                $restaurentId = $checkValidId->restaurent_id;

                $itemConditions = array();
                if ($restaurentId) {
                    $itemConditions['rul.restaurants_user_location_res_id'] = $restaurentId;
                }

                $restaurentData = $this->query_model->getRow("restaurents", array("restaurents.restaurent_id" => $restaurentId));
                $columns = array(
                    "user.*",
                    "rul.*",
                    "dr.driver_id",
                    "dr.request_status",
                );
                $orderBy = array(
                    array("rul.created_at", "DESC")
                );
                $joins = array(
                    array("user", "user.user_id=rul.restaurants_user_location_user_id", "inner"),
                    array("driver_requests as dr", "dr.restaurants_user_location_id=rul.restaurants_user_location_id", "left")
                );

                $result = $this->query_model->getRows("restaurants_user_location as rul", $itemConditions, $columns, $orderBy, $joins);
                foreach ($result as $resObj) {
                    $tz = new DateTimeZone('Europe/Rome');

                    $today = new DateTime($resObj->created_at);
                    $today->setTimezone($tz);
                    $resObj->created_at = $today->format('Y-m-d H:i:s');
                }


                if ($result) {
                    $status = true;
                    $resData = $result;
                } else {
                    $message = "No data found.";
                }
            } else {
                $message = "Restaurent id is invalid.";
            }
        }
        $this->response(array(
            'restaurent' => $restaurentData,
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function getDrivers_get()
    {
        $status = false;
        $message = "";
        $resData = null;
        $restaurentData = null;

        $restaurentId = $this->input->get('restaurent_id');
        $driver_id = $this->input->get('driver_id');


        if (!isset($restaurentId)) {
            $message = "restaurent_id is required";
        } else {
            $checkValidId = $this->query_model->getRow("restaurents", array("restaurent_id" => $restaurentId));
            if ($checkValidId) {
                $restaurentId = $checkValidId->restaurent_id;

                $conditions = array(
                    "is_online" => 1
                );
                if (!empty($driver_id)) {
                    $conditions['user.user_id'] = $driver_id;
                }
                $columns = array(
                    "dlt.*",
                    "user.*",
                );
                $orderBy = array(
                    array("dlt.created_at", "DESC")
                );
                $joins = array(
                    array("user", "user.user_id=dlt.user_id", "inner"),
                );

                $result = $this->query_model->getRows("driver_location_track as dlt", $conditions, $columns, $orderBy, $joins);

                if ($result) {
                    $status = true;
                    $resData = $result;
                } else {
                    $message = "No data found.";
                }
            } else {
                $message = "Restaurent id is invalid.";
            }
        }
        $this->response(array(
            'restaurent' => $restaurentData,
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function saveLocation_post()
    {
        $userId = $this->post('user_id');
        $locationType = $this->post('location_type');//H,O
        $locationAddress = $this->post('location_address');
        $locationLat = $this->post('location_lat');
        $locationLng = $this->post('location_lng');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($userId)) {
            $message = "user_id is required";
        } elseif (!isset($locationType)) {
            $message = "location_type is required";
        } elseif (!isset($locationAddress)) {
            $message = "location_address is required";
        } elseif (!isset($locationLat)) {
            $message = "location_lat is required";
        } elseif (!isset($locationLng)) {
            $message = "location_lng is required";
        } else {
            $result = $this->updateUserAddress($locationAddress, $locationLat, $locationLng, $userId, $locationType);

            if ($result) {
                $conditions = array(
                    "location_user_id" => $userId,
                );
                $resData = $this->query_model->getRows("user_location", $conditions);
                $status = true;
            } else {
                $message = "Something went wrong";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function sendDriverRequest_post()
    {
        $senderId = $this->post('sender_id');
        $driverId = $this->post('driver_id');//H,O
        $restaurentId = $this->post('restaurent_id');
        $restaurentUserLocationId = $this->post('restaurants_user_location_id');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($senderId)) {
            $message = "sender_id is required";
        } elseif (!isset($driverId)) {
            $message = "driver_id is required";
        } elseif (!isset($restaurentId)) {
            $message = "restaurent_id is required";
        } elseif (!isset($restaurentUserLocationId)) {
            $message = "restaurants_user_location_id is required";
        } else {
            $reqCond = array(
                "restaurants_user_location_id" => $restaurentUserLocationId,
                "request_status != " => 'R',
            );
            $req = $this->query_model->getRow("driver_requests", $reqCond);
            if ($req) {
                $message = "Request already sent for this user";
            } else {
                $reqData = array(
                    "driver_id" => $driverId,
                    "sender_id" => $senderId,
                    "restaurent_id" => $restaurentId,
                    "restaurants_user_location_id" => $restaurentUserLocationId,
                );
                $res = $this->query_model->insertRow("driver_requests", $reqData);
                if ($res) {
                    $status = true;
                    $resData = $res;
                    $message = "Request successfully sent";
                } else {
                    $message = "Something went wrong";
                }
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function driverRequests_get()
    {
        $driverId = $this->get('driver_id');//H,O

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($driverId)) {
            $message = "driver_id is required";
        } else {
            $reqCond = array(
                "driver_id" => $driverId,
//                "request_status" => 'P',
            );
            $reqCol = array(
                "dr.*",
                "restaurents.restaurent_name",
                "restaurents.restaurent_partner_name",
                "restaurents.restaurent_address",
                "restaurents.restaurent_latitude",
                "restaurents.restaurent_longitude",
                "user.user_first",
                "user.user_phone",
                "sender.user_first as sender_name",
                "sender.user_phone as sender_phone",
                "sender.push_token as sender_push_token",
                "rul.restaurants_user_location_lat",
                "rul.restaurants_user_location_lng",
            );
            $joins = array(
                array("restaurents", "restaurents.restaurent_id=dr.restaurent_id", "inner"),
                array("restaurants_user_location as rul", "rul.restaurants_user_location_id=dr.restaurants_user_location_id", "inner"),
                array("user", "user.user_id=rul.restaurants_user_location_user_id", "inner"),
                array("user as sender", "sender.user_id=dr.sender_id", "inner"),
            );
            $reqs = $this->query_model->getRows("driver_requests as dr", $reqCond, $reqCol, null, $joins);
            if ($reqs) {
                $status = true;
                $resData = $reqs;
            } else {
                $message = "No driver requests found.";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function driverRequestUpdate_post()
    {
        $driverId = $this->post('driver_id');//H,O
        $driverRequestId = $this->post('driver_request_id');//H,O
        $requestStatus = $this->post('request_status');//H,O

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($driverId)) {
            $message = "driver_id is required";
        } else {
            $reqCond = array(
                "id" => $driverRequestId,
            );
            $reqData = array(
                "request_status" => $requestStatus
            );

            $res = $this->query_model->updateRow("driver_requests as dr", $reqCond, $reqData);

            if ($res) {
                $status = true;
                $resData = $res;
            } else {
                $message = "No driver requests found.";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }


    public function updateDriverLocation_post()
    {
        $userId = $this->post('user_id');
        $locationLat = $this->post('location_lat');
        $locationLng = $this->post('location_lng');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($userId)) {
            $message = "user_id is required";
        } elseif (empty($locationLat)) {
            $message = "location_lat is required";
        } elseif (empty($locationLng)) {
            $message = "location_lng is required";
        } else {
            $driverLocData = array(
                "user_id" => $userId,
                "latitude" => $locationLat,
                "longitude" => $locationLng,
            );
            $checkUserCond = array("user_id" => $userId);
            $checkUser = $this->query_model->getRow("driver_location_track", $checkUserCond);
            if ($checkUser) {
                $locUpdateRes = $this->query_model->updateRow("driver_location_track", $checkUserCond, $driverLocData);
            } else {
                $locUpdateRes = $this->query_model->insertRow("driver_location_track", $driverLocData);
            }

            if ($locUpdateRes) {
                $resData = $this->query_model->getRow("driver_location_track", $checkUserCond);
                $status = true;
                $message = "Driver location updated";
            } else {
                $message = "Something went wrong";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    public function updateDriverStatus_post()
    {
        $userId = $this->post('user_id');
        $driverStatus = $this->post('status');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($userId)) {
            $message = "user_id is required";
        } else {
            $driverLocData = array(
                "user_id" => $userId,
                "is_online" => $driverStatus,
            );
            $checkUserCond = array("user_id" => $userId);
            $checkUser = $this->query_model->getRow("driver_location_track", $checkUserCond);
            if ($checkUser) {
                $locUpdateRes = $this->query_model->updateRow("driver_location_track", $checkUserCond, $driverLocData);
            } else {
                $locUpdateRes = $this->query_model->insertRow("driver_location_track", $driverLocData);
            }

            if ($locUpdateRes) {
                $resData = $locUpdateRes;
                $status = true;
                $message = "Driver status updated";
            } else {
                $message = "Something went wrong";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }


    public function sendLocation_post()
    {
        $userId = $this->post('user_id');
        $locationAddress = $this->post('location_address');//H,O
        $locationLat = $this->post('location_lat');//H,O
        $locationLng = $this->post('location_lng');//H,O
        $resId = $this->post('restaurent_id');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($userId)) {
            $message = "user_id is required";
        } elseif (!isset($locationLat)) {
            $message = "location_lat is required";
        } elseif (!isset($locationLng)) {
            $message = "location_lng is required";
        } elseif (!isset($resId)) {
            $message = "restaurent_id is required";
        } else {
            $locData = array(
                "restaurants_user_location_user_id" => $userId,
                "restaurants_user_location_res_id" => $resId,
                "restaurants_user_location_lat" => $locationLat,
                "restaurants_user_location_lng" => $locationLng,
                "restaurants_user_location_address" => $locationAddress,
            );
            $result = $this->query_model->insertRow("restaurants_user_location", $locData);
            if ($result) {
                $status = true;
                $message = "Location sent success";
            } else {
                $message = "Something went wrong";
            }
        }
        $this->response(array(
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ), 200);
    }

    private function updateUserAddress($userAddress, $userLat, $userLng, $userId, $addressType)
    {
        $conditions = array(
            "location_user_id" => $userId,
            "location_type" => $addressType,
        );
        $data = array(
            "location_address" => $userAddress,
            "location_lat" => $userLat,
            "location_lng" => $userLng,
            "location_user_id" => $userId,
            "location_type" => $addressType,
        );
        $checkAlready = $this->query_model->getRow("user_location", $conditions);
        if ($checkAlready) {
            $this->query_model->updateRow("user_location", $conditions, $data);
        } else {
            $this->query_model->insertRow("user_location", $data);
        }
        return true;
    }

    public function getMyLocations_get()
    {
        $userId = $this->input->get('user_id');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($userId)) {
            $message = "user_id is required";
        } else {
            $conditions = array(
                "location_user_id" => $userId,
            );
            $result = $this->query_model->getRows("user_location", $conditions);
            if ($result) {
                $status = true;
                $resData = $result;
            } else {
                $message = "No locations found";
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }
}
