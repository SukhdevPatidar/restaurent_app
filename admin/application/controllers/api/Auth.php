<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->model('email_model');
        $this->load->helper('URL');
    }

    public function index_get()
    {
        $this->response([
            'status' => false,
            'message' => "unknown method"
        ], 200);
    }


    public function checkMobileNo_post()
    {
        $mobileNumber = $this->post('mobile');
        $user_type = $this->post('user_role');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($mobileNumber)) {
            $message = "mobile is required";
        } else {
            $condition = array(
                "user_phone" => $mobileNumber,
            );
            if ($user_type) {
                $condition['user_role'] = $user_type;
            } else {
                $condition['user_role'] = 2;
            }

            $checkAlready = $this->query_model->getRow("user", $condition);
            if ($checkAlready) {
                $status = true;
                $resData = $checkAlready;
            } else {
                $message = "User not found";
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

    public function registerphone_post()
    {
        $firstName = $this->post('fname');
        $lastName = $this->post('lname');
        $mobileNumber = $this->post('mobile');
        $city = $this->post('city');
        $userType = $this->post('user_role');
        $base64Image = $this->post('user_photo'); // base64

        $status = false;
        $message = "";
        $resData = null;

        if (empty($firstName)) {
            $message = "fname is required";
        } elseif (empty($mobileNumber)) {
            $message = "mobile is required";
        } else {
            $condition = array("user_phone" => $mobileNumber);
            if (!empty($userType)) {
                $condition['user_role'] = $userType;
            } else {
                $condition['user_role'] = 2;
            }

            $checkAlready = $this->query_model->getRow("user", $condition);

            $password = $this->generatePassword();
            $data = array(
                'user_first' => $firstName,
                'user_last' => $lastName,
                'user_city_id' => $city,
                'user_password' => md5($password),
                'user_phone' => $mobileNumber,
            );
            if (!empty($userType)) {
                $data['user_role'] = $userType;
            } else {
                $data['user_role'] = 2;
            }


            $photoPath = "";
            $isPhoto = false;
            if ($base64Image) {
                $isPhoto = true;
                if (base64_encode(base64_decode($base64Image, true)) === $base64Image) {
                    $dir = './assets/uploads/profile';
                    if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    if ($checkAlready && $checkAlready->user_photo) {
                        unlink($checkAlready->user_photo);
                    }

                    $image_name = md5(uniqid(rand(), true));
                    $decoded = base64_decode($base64Image);
                    $fileName = $dir . '/' . $image_name . '.png';
                    $res = file_put_contents($fileName, $decoded);
                    $photoPath = $dir . '/' . $image_name . '.png';
                } else {
                    $message = "photo should be base64 string";
                }
            }

            if (($isPhoto && $photoPath) || !$isPhoto) {
                if ($photoPath) {
                    $data['user_photo'] = $photoPath;
                }
            }

            $result = null;
            if ($checkAlready) {
                $result = $this->query_model->updateRow("user", $condition, $data);
            } else {
                $result = $this->query_model->insertRow("user", $data);
            }

            if ($result) {
                $resData = $this->query_model->getRow("user", $condition);
                $status = true;
                $message = "User register successfully.";
            } else {
                $message = "Something went wrong";
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

    public function forgotPassword_post()
    {
        $username = $this->input->post('email');

        $status = false;
        $message = "";
        $resData = null;

        if (!isset($username)) {
            $message = "username is required";
        } else {
            $result = $this->query_model->getRow("user", array("user_phone" => $username));
            if (!$result) {
                $result = $this->query_model->getRow("user", array('user_mail' => $username));
            }

            if (!$result) {
                $message = "Account not found";
            } else {
                $password = $this->generatePassword();
                $message = "You new password is " . $password;
//                $resetCode = mt_rand(1111111111111111, 9999999999999999) . mt_rand(1111111111111111, 9999999999999999);
                $res = $this->query_model->updateRow("user", array("user_id" => $result->user_id), array("user_password" => md5($password)));
//                $message = "Click on this link to set new password <a href='" . base_url() . "resetPassword?resetid=" . $resetCode . "'>Reset Password</a>";


                if ($this->email_model->sendEmail($result->user_mail, 'Reset Password', $message)) {
                    $status = true;
                    $message = "New password sent successfully";
                } else {
                    $message = "Something went wrong";
                }
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

    public function changePassword_post()
    {
        $userId = $this->input->post('user_id');
        $oldPassword = $this->input->post('old_password');
        $newPassword = $this->input->post('new_password');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($userId)) {
            $message = "user_id is required";
        } elseif (empty($oldPassword)) {
            $message = "old_password is required";
        } elseif (empty($newPassword)) {
            $message = "new_password is required";
        } else {
            $data = array(
                "user_id" => $userId,
                "user_password" => $oldPassword,
            );
            $result = $this->query_model->getRow("user", $data);
            if ($result) {
                $update = array("user_password" => $newPassword);
                $updateResult = $this->query_model->updateRow("user", array("user_id" => $userId), $update);
                if ($updateResult) {
                    $status = true;
                    $message = "Password successfully updated";
                } else {
                    $message = "Something went wrong";
                }
            } else {
                $message = "Old password is wrong";
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

    private function generatePassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    public function getUser_post()
    {
        $userId = $this->input->post('user_id');

        $status = false;
        $message = "";
        $resData = null;

        if (empty($userId)) {
            $message = "user_id is required";
        } else {
            $user = $this->query_model->getRow("user", array("user_id" => $userId));
            if ($user) {
                $status = true;
                $resData = $user;
            } else {
                $message = "User not found";
            }
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

    public function updateProfile_post()
    {
        $userId = $this->post('user_id');
        $userFirst = $this->post('user_first');
        $userCityId = $this->post('user_city_id');
        $userPhone = $this->post('user_phone');
        $pushToken = $this->post('push_token');
        $base64Image = $this->post('user_photo'); // base64

        $message = "";
        $resData = null;
        $status = false;

        if (!$userId) {
            $message = "user_id is required";
        } else {
            $photoPath = "";
            $isPhoto = false;

            $userCond = array("user_id" => $userId);

            if ($base64Image) {
                $isPhoto = true;
                if (base64_encode(base64_decode($base64Image, true)) === $base64Image) {
                    $dir = './assets/uploads/profile';
                    if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    $user = $this->query_model->getRow("user", $userCond);

                    if ($user && $user->user_photo) {
                        unlink($user->user_photo);
                    }

                    $image_name = md5(uniqid(rand(), true));
                    $decoded = base64_decode($base64Image);
                    $fileName = $dir . '/' . $image_name . '.png';
                    $res = file_put_contents($fileName, $decoded);
                    $photoPath = $dir . '/' . $image_name . '.png';
                } else {
                    $message = "photo should be base64 string";
                }
            }

            if (($isPhoto && $photoPath) || !$isPhoto) {
                $data = array();
                if ($userFirst) {
                    $data['user_first'] = $userFirst;
                }
                if ($userCityId) {
                    $data['user_city_id'] = $userCityId;
                }

                if ($userPhone) {
                    $data['user_phone'] = $userPhone;
                }

                if ($pushToken) {
                    $data['push_token'] = $pushToken;
                }

                if ($photoPath) {
                    $data['user_photo'] = $photoPath;
                }

                $this->query_model->updateRow('user', $userCond, $data);
                $resData = $this->query_model->getRow("user", $userCond);
                if ($resData) {
                    $status = true;
                }
            }
        }

        $this->response([
            'status' => $status,
            'message' => $message,
            'data' => $resData
        ], 200);
    }
}
