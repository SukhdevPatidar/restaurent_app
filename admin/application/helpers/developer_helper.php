<?php

function findValue($listName, $val)
{
    $list = json_decode(constant($listName));
    $outputStr = "-";
    foreach ($list as $key => $value) {
        if ($key == $val) {
            $outputStr = $value;
            break;
        }
    }
    return $outputStr;
}

function vehicleType($type)
{
    return findValue("VehicleType", $type);
}

function vehicleTypeList()
{
    return json_decode(constant("VehicleType"));
}

//deliveryStatus
function deliveryStatus($status)
{
    return findValue("DeliveryStatus", $status);
}

function deliveryStatusList()
{
    return json_decode(constant("DeliveryStatus"));
}

//deliveryStatus
function orderStatus($status)
{
    return findValue("OrderStatus", $status);
}

function orderStatusList()
{
    return json_decode(constant("OrderStatus"));
}

//deliverySpeedType
function deliverySpeedType($type)
{
    return findValue("DeliverySpeed", $type);
}

function deliverySpeedTypeList()
{
    return json_decode(constant("DeliverySpeed"));
}

//deliveryAreaType
function deliveryAreaType($type)
{
    return findValue("DeliveryAreaType", $type);
}

function deliveryAreaTypeList()
{
    return json_decode(constant("DeliveryAreaType"));
}

//userStatus
function userStatus($status)
{
    return findValue("UserStatus", $status);
}

function userStatusList()
{
    return json_decode(constant("UserStatus"));
}

//appType
function subscriptionType($type)
{
    return findValue("SubscriptionType", $type);
}

function subscriptionTypeList()
{
    return json_decode(constant("SubscriptionType"));
}

//PaymentMode
function paymentMode($type)
{
    return findValue("PaymentMode", $type);
}

function paymentModeList()
{
    return json_decode(constant("PaymentMode"));
}

//PaymentFor
function paymentFor($type)
{
    return findValue("PaymentFor", $type);
}

function paymentForList()
{
    return json_decode(constant("PaymentFor"));
}

//PaymentType
function paymentType($type)
{
    return findValue("PaymentType", $type);
}

function paymentTypeList()
{
    return json_decode(constant("PaymentType"));
}

//appType
function appType($type)
{
    return findValue("AppType", $type);
}

function appTypeList()
{
    return json_decode(constant("AppType"));
}

// date and time
function getDateFormat($timeStr)
{
    if (!empty($timeStr)) {
        return date("d-m-Y", strtotime($timeStr));
    }
    return "_";
}

function getTimeFormat($timeStr)
{
    if (!empty($timeStr)) {
        return date("h:i A", strtotime($timeStr));
    }
    return "_";
}


function _check_user_login()
{
    if (check_user_login() === FALSE) redirect('signin');
}

function _check_auto_login()
{
    if (check_user_login()) {
        redirect("admin/dashboard");
    }
    return;
}

function sideMenuItemVisibility($menuItem)
{
    $agencyMenu = array("send", "sell", "employee", "payment");
    $superAdminMenu = array("home", "users", "send", "sell", "commission", "agency", "employee", "smartshop");
    $CI = &get_instance();
    $user = $CI->session->userdata('user_data');
    if ($user->user_role == 3) { //Rescategory
        if (in_array($menuItem, $agencyMenu)) {
            return "block";
        } else {
            return "none";
        }
    } else if ($user->user_role == 1) { //Super Admin
        if (in_array($menuItem, $superAdminMenu)) {
            return "block";
        } else {
            return "none";
        }
    }
    return "block";
}

if (!function_exists('check_user_login')) {

    function check_user_login()
    {
        $CI = &get_instance();
        $user_info = $CI->session->userdata('user_data');
        //&& $user_info['user_role']=='manager'
        if (!empty($user_info)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('upload_file')) {

    function upload_file($param = null)
    {

        $CI = &get_instance();

        $config['upload_path'] = './assets/uploads/';

        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|jpeg|pdf|doc|docx|JPG|PNG';

        $config['max_size'] = 1024 * 90;

        $config['image_resize'] = TRUE;

        $config['resize_width'] = 200;

        $config['resize_height'] = 200;

        if ($param) {
            $config = $param + $config;
        }

        $CI->load->library('upload');

        $CI->upload->initialize($config);

        if (!empty($config['file_name'])) $file_Status = $CI->upload->do_upload($config['file_name']);

        else $file_Status = $CI->upload->do_upload();

        if (!$file_Status) {

            return array('STATUS' => FALSE, 'FILE_ERROR' => $CI->upload->display_errors());

        } else {


            $uplaod_data = $CI->upload->data();

            // if(empty($param['resize_width'])&&empty($param['resize_height'])){
            //      // $original_height = $uplaod_data['image_height'];
            //      // $original_width =  $uplaod_data['image_width'];
            //         $config['resize_width']=175;
            //         $config['resize_height']=150;
            //  }

            $upload_file = explode('.', $uplaod_data['file_name']);


            if ($config['image_resize'] && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {

                $param2 = array(

                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],

                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],

                    'create_thumb' => FALSE,

                    'maintain_ratio' => TRUE,

                    'width' => $config['resize_width'],

                    'height' => $config['resize_height'],

                );


                image_resize($param2);

            }

            if (empty($config['image_resize']) && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {

                $param3 = array(
                    'file_name' => $uplaod_data['file_name'],

                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],

                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],

                    'maintain_ratio' => 0,

                    'width' => $config['resize_width'],

                    'height' => $config['resize_height'],

                );

                create_frontend_thumbnail($param3);

            }
            return array('STATUS' => TRUE, 'UPLOAD_DATA' => $uplaod_data);

        }

    }


}

function create_frontend_thumbnail($config_img = '')

{


    $CI = &get_instance();

    $config_image['image_library'] = 'gd2';

    $config_image['source_image'] = $config_img['source_image'];

    $config_image['file_name'] = $config_img['file_name'];

    $config_image['new_image'] = $config_img['new_image'];

    $config_image['height'] = $config_img['height'];

    $config_image['width'] = $config_img['width'];


    list($width, $height, $type, $attr) = getimagesize($config_image['source_image']);

    if ($width < $height) {

        $cal = $width / $height;

        $config_image['width'] = $config_img['width'] * $cal;

    }

    if ($height < $width) {

        $cal = $height / $width;

        $config_image['height'] = $config_img['height'] * $cal;

    }

    $CI->load->library('image_lib');
    $CI->image_lib->initialize($config_image);
    if (!$CI->image_lib->resize()) return array('status' => FALSE, 'error_msg' => $CI->image_lib->display_errors());

    else return array('status' => TRUE, 'file_name' => $config_image['file_name']);

}

?>
