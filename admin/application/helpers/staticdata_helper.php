<?php
define("DeliveryStatus", json_encode(array(
    "P" => "Processing",
    "T" => "Picked",
    "D" => "Dispatched",
    "C" => "Canceled",
    "F" => "Delivered",
)));

define("OrderStatus", json_encode(array(
    "P" => "Waiting",
    "D" => "Dispatched",
    "C" => "Canceled",
    "F" => "Delivered",
)));

define("VehicleType", json_encode(array(
    "B" => "Bike",
    "C" => "Car",
    "A" => "All",
)));


define("DeliveryAreaType", json_encode(array(
    "I" => "In City",
    "O" => "Out City",
)));

define("DeliverySpeed", json_encode(array(
    "F" => "Fast",
    "N" => "Normal",
)));

define("UserStatus", json_encode(array(
    "A" => "Active",
    "P" => "Passive",
)));

define("SubscriptionType", json_encode(array(
    'M' => "Monthly",
    'Y' => "Yearly",
)));


define("PaymentMode", json_encode(array(
    0 => "Online",
    1 => "COD",
)));

define("PaymentFor", json_encode(array(
    1 => "Delivery",
    2 => "Smartshop",
)));

define("PaymentType", json_encode(array(
    "P" => "Paid",
    "R" => "Refund",
)));

define("AppType", json_encode(array(
    0 => "Normal App",
    1 => "Smartshop App",
)));

?>
