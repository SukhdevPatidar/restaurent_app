<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
    }

    public function getDeliveryReport($moreConditions = null)
    {
        $columns = array(
            "delivery.*",
            "dl.delivery_log_created                    as delivery_date",
            "user.user_first                            as sender_fname",
            "user.user_last                             as sender_lname",
            "user.user_phone                            as sender_phone",
            "da_sender.delivery_address_latitude        as sender_latitude",
            "da_sender.delivery_address_longitude       as sender_longitude",
            "da_sender.delivery_address_address         as sender_address",
            "da_sender.delivery_address_moreaddress     as sender_moreaddress",
            "da_sender.delivery_address_type            as sender_address_type",
            "da_reciever.delivery_address_contact_name  as reciever_contact_name",
            "da_reciever.delivery_address_contact_phone as reciever_contact_phone",
            "da_reciever.delivery_address_latitude      as reciever_latitude",
            "da_reciever.delivery_address_longitude     as reciever_longitude",
            "da_reciever.delivery_address_address       as reciever_address",
            "da_reciever.delivery_address_moreaddress   as reciever_moreaddress",
            "da_reciever.delivery_address_type          as reciever_address_type",
        );
        $conditions = array(
            "da_sender.delivery_address_user_type" => 1,
            "da_reciever.delivery_address_user_type" => 2,
        );
        if (is_array($moreConditions)) {
            $conditions = array_merge($conditions, $moreConditions);
        }

        $orderBy = array(array("delivery.delivery_id", "desc"));
        $joins = array(
            array("user", "user.user_id=delivery.delivery_from_user_id", "inner"),
            array("delivery_address as da_sender", "da_sender.delivery_address_delivery_id=delivery.delivery_id", "inner"),
            array("delivery_address as da_reciever", "da_reciever.delivery_address_delivery_id=delivery.delivery_id", "inner"),
            array("delivery_log as dl", "dl.delivery_id=delivery.delivery_id AND dl.delivery_status='F' AND delivery_log_type='D'", "left"),
        );
        $list = $this->query_model->getRows("delivery", $conditions, $columns, $orderBy, $joins);
        return $list;
    }


    public function logDeliveryStatus($deliveryId, $status, $logType)
    {
        $logData = array(
            "delivery_id" => $deliveryId,
            "delivery_status" => $status,
            "delivery_log_type" => $logType,
        );
        $check = $this->query_model->getRow("delivery_log", $logData);
        if($check) {
            $resultLog = $this->query_model->updateRow(array("delivery_log_id" => $check->delivery_log_id), $logData, $logData);
        } else {
            $resultLog = $this->query_model->insertRow("delivery_log", $logData);
        }
        return $resultLog;
    }

}

?>