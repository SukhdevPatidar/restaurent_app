<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model
{
    public function sendEmail($email, $subject, $message) {
//        $config = array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://mail.snappypanel.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'support@snappypanel.com',
//            'smtp_pass' => 'snappy@L123',
//            'mailtype'  => 'html'
//        );
//        $this->load->library('email', $config);
        $this->load->library('email');
        $this->email->from('info@smartpoint.com', 'SmartPoint');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        return $this->email->send();
    }

}
?>