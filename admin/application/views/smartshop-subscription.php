<?php $this->load->view('admin/template/auth/header'); ?>
    <body class="signup-page">
    <div class="signup-box">
        <?php $this->load->view('admin/template/auth/logo'); ?>

        <div class="card">
            <div class="body">
                <form id="sign_up" method="POST">
                    <input type="hidden" name="user_id" required value="<?php echo $userId; ?>">

                    <div class="msg">Subscribe Smartshoping</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="fname" placeholder="Name" required
                                   autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="lname" placeholder="Surname" required
                                   autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div class="form-line">
                            <input type="number" class="form-control" name="mobile" placeholder="mobile" required
                                   autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                        </div>
                    </div>


                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Subscribe</button>


                </form>
            </div>
        </div>
    </div>
    </body>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/admin/js/pages/auth/sign-up.js"></script>

<?php $this->load->view('admin/template/auth/footer'); ?>