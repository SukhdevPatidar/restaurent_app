<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>

<?php
if ($type == "add" || $type == "edit") {
  ?>

  <section class="content">
    <div class="container-fluid">

      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <div class="card">
            <div class="header">
              <h2>Restaurent </h2>
            </div>
            <div class="body">

              <form id="agencyForm" method="POST"
              enctype="multipart/form-data"
              action="<?php echo htmlspecialchars(base_url('admin/restaurents/addRestaurent')); ?>">

              <input type="hidden" name="restaurent_id"
              value="<?php
              echo ($type == 'edit') ? $data->restaurent_id : ''; ?>"/>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="text" class="form-control" name="restaurentName"
                  required
                  value="<?php echo ($type == 'edit') ? $data->restaurent_name : ''; ?>">
                  <label class="form-label"> Restaurent Name</label>
                </div>
                <div class="help-info"></div>
              </div>
              <!--<div class="form-group form-float">
              <div class="form-line">
              <input type="text" class="form-control" name="restaurentCategoryId"
              required
              value="<?php /*echo ($type == 'edit') ? $data->restaurent_category_id : ''; */ ?>">
              <label class="form-label"> Restaurent Category</label>
            </div>
            <div class="help-info"></div>
          </div>-->


          <div class="form-group">
            <label for="terms">Restaurent Category</label>

            <div class="form-line">


              <?php
              $catIds = array();
              if (isset($data)) {
                foreach ($data->categories as $cat) {
                  $catIds[] = $cat->category_id;
                }
              }


              foreach ($resto as $key => $s):


                if ($type == 'edit' && in_array($s->category_id, $catIds)) {
                  ?>

                  <input type="checkbox" name="restaurentCategoryId[]"
                  id="cat_<?= $key; ?>" class="filled-in chk-col-pink"
                  value="<?= $s->category_id ?>" checked>
                  <label for="cat_<?= $key; ?>"><?php echo $s->category_name ?></label>
                  <?php
                } else {
                  ?>
                  <input type="checkbox" name="restaurentCategoryId[]"
                  id="cat_<?= $key; ?>" class="filled-in chk-col-pink"
                  value="<?= $s->category_id ?>">
                  <label for="cat_<?= $key; ?>"><?php echo $s->category_name ?></label>
                  <?php
                } ?>
              <?php endforeach ?>

            </div>
            <div class="help-info"></div>
          </div>
          <!-- <div class="form-group form-float">
          <div class="form-line">
          <input type="text" class="form-control" name="restaurentCityId"
          required
          value="<?php /*echo ($type == 'edit') ? $data->restaurent_city_id : ''; */ ?>">
          <label class="form-label"> Restaurent City</label>
        </div>
        <div class="help-info"></div>
      </div>-->
      <div class="form-group">
        <div class="form-line">
          <select class="form-control show-tick" name="restaurentCityId"
          id="restaurentCityList"
          required>
          <option value=''>Select City</option>
          <?php
          foreach ($cities as $value) {
            if ($type == 'edit' && $value->city_id == $data->restaurent_city_id) {
              echo "<option value='$value->city_id' selected>$value->city_name</option>";
            } else {
              echo "<option value='$value->city_id'>$value->city_name</option>";
            }
          } ?>

        </select>
      </div>
      <div class="help-info"></div>
    </div>
    <?php

    ?>

    <div class="form-group form-float">
      <div class="form-line">
        <input type="text" class="form-control" name="restaurentPartnerName"
        required
        value="<?php echo ($type == 'edit') ? $data->restaurent_partner_name : ''; ?>">
        <label class="form-label"> Restaurent Partner</label>
      </div>
      <div class="help-info"></div>
    </div>
    <div class="form-group form-float">
      <div class="form-line">
        <input type="number" class="form-control" name="restaurentPhone"
        required
        value="<?php echo ($type == 'edit') ? $data->restaurent_phone : ''; ?>">
        <label class="form-label"> Restaurent Phone</label>
      </div>
      <div class="help-info"></div>
    </div>
    <div class="form-group form-float">
      <div class="form-line">
        <input type="file" id="resPhoto" class="form-control" name="photo"

        value="<?php echo ($type == 'edit') ? $data->restaurent_partner_photo : ''; ?>"/>
        <label class="form-label"> Restaurent Partner</label>
      </div>
      <div class="help-info"></div>
    </div>

    <div class="form-group form-float">
      <div class="form-line">
        <input type="hidden" class="form-control" name="restaurentLat" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_latitude : ''; ?>">
        <input type="hidden" class="form-control" name="restaurentLng" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_longitude : ''; ?>">
        <input type="text" class="form-control" name="restaurentAddress" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_address : ''; ?>">
        <label class="form-label">Address</label>
      </div>
      <div class="help-info"></div>
    </div>

    <div class="form-group">
      <div id="locationMap" style="width: 100%; height: 500px"
      latitude="<?php echo ($type == 'edit') ? $data->restaurent_latitude : ''; ?>"
      longitude="<?php echo ($type == 'edit') ? $data->restaurent_longitude : ''; ?>"></div>
    </div>

    <div class="form-group">
      <?php
      $index = 0;
      foreach (userStatusList() as $key => $value) {
        if (($type == 'edit' && $data->restaurent_status == $key) || ($type != 'edit' && $index == 0)) {
          echo "<input type='radio' name='restaurentStatus' value='$key' id='restaurent$key' class='with-gap' checked>";
        } else {
          echo "<input type='radio' name='restaurentStatus' value='$key' id='restaurent$key' class='with-gap'>";
        }
        echo "<label for='restaurent$key'>$value</label>";
        $index++;
      } ?>
    </div>

    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
  </form>
</div>
</div>
</div>
</div>
</div>
</section>

<?php
} else {
  ?>
  <section class="content">
    <div class="container-fluid">
      <!-- Basic Examples -->
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <div class="body">
            <a href="<?php echo htmlspecialchars(base_url('admin/restaurents?type=add')); ?>">
              <button type="button" class="btn btn-info btn-lg waves-effect">Add Restaurents
              </button>
            </a>
          </div>

          <br/>


          <div class="card">
            <div class="header">
              <h2>
                Restaurents
              </h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover agencyTable dataTable">
                  <thead>
                    <tr>
                      <th>Restaurents Code</th>
                      <th>Restaurents Name</th>
                      <th>Restaurents Category</th>
                      <th>Restaurents Partner</th>
                      <th>Partner Photo</th>
                      <th>City</th>
                      <th>Phone</th>

                      <th>Action</th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php
                    if (isset($list) && !empty($list)) {
                      foreach ($list as $key => $dataObj) {
                        ?>
                        <tr>
                          <td><?php echo $dataObj->restaurent_code; ?></td>
                          <td><?php echo $dataObj->restaurent_name; ?></td>
                          <td><?php
                          $catNames = array();
                          foreach ($dataObj->categories as $cat) {
                            $catNames[] = $cat->category_name;
                          }
                          echo implode(", ", $catNames); ?>
                        </td>
                        <td><?php echo $dataObj->restaurent_partner_name; ?></td>
                        <td>
                          <img src="<?php echo base_url(); ?><?php echo $dataObj->restaurent_partner_photo; ?>"
                          height="50" width="50"/>
                        </td>
                        <td><?php echo $dataObj->city_name; ?></td>
                        <td><?php echo $dataObj->restaurent_phone; ?></td>

                        <td class="button-demo">
                          <a href="<?php echo htmlspecialchars(base_url('admin/menupreview?res_id=' . $dataObj->restaurent_id)); ?>">
                            <button type="button" class="btn btn-info btn-sm waves-effect">
                              Preview
                            </button>
                          </a>

                          <a href="<?php echo htmlspecialchars(base_url('admin/menucategory?res_id=' . $dataObj->restaurent_id)); ?>">
                            <button type="button"
                            class="btn btn-primary btn-sm waves-effect">
                            Menus
                          </button>
                        </a>

                        <a href="<?php echo htmlspecialchars(base_url('admin/restaurents?type=edit&id=' . $dataObj->restaurent_id)); ?>">
                          <button type="button"
                          class="btn btn-warning btn-sm waves-effect">
                          Edit
                        </button>
                      </a>

                      <a href="<?php echo htmlspecialchars(base_url('admin/restaurents/deleteRestaurent?id=' . $dataObj->restaurent_id)); ?>">
                        <button type="button"
                        class="btn btn-danger btn-sm waves-effect">
                        Delete
                      </button>
                    </a>
                  </td>
                </tr>
                <?php
              }
            } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</section>
<?php
}
?>

<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/agency.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
async defer></script>
