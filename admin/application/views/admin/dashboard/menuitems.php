<?php
if ($type == "add" || $type == "edit") {
    ?>

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>Menu Item </h2>
                        </div>
                        <div class="body">
                            <form id="agencyForm" method="POST"
                                  enctype="multipart/form-data"

                                  action="<?php echo htmlspecialchars(base_url('admin/menuitems/addItems')); ?>">

                                <input type="hidden" name="menuId"
                                       value="<?= $menu_id ?>"/>
                                <input type="hidden" name="resId"
                                       value="<?= $res_id; ?>"/>
                                <input type="hidden" name="itemId"
                                       value="<?php
                                       echo ($type == 'edit') ? $data->item_id : ''; ?>"/>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="itemName"
                                               required
                                               value="<?php echo ($type == 'edit') ? $data->item_name : ''; ?>">
                                        <label class="form-label"> Item Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="itemPrice"
                                               required
                                               value="<?php echo ($type == 'edit') ? (number_format((float)$data->item_price, 2, '.', '')) : ''; ?>">
                                        <label class="form-label"> Item Price</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="itemDescription"
                                               value="<?php echo ($type == 'edit') ? $data->item_description : ''; ?>">
                                        <label class="form-label"> Item Description</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" id="resPhoto" class="form-control" name="photo"

                                               value="<?php echo ($type == 'edit') ? $data->item_photo : ''; ?>"/>
                                        <label class="form-label"> Restaurent Partner</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
} else {
    ?>
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="body">
                        <div class="button-demo">
                            <a href="<?php echo htmlspecialchars(base_url('admin/menucategory?res_id=' . $res_id)); ?>">
                                <button type="button" class="btn btn-default waves-effect">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    <span>BACK</span>
                                </button>
                            </a>

                            <a href="<?php echo htmlspecialchars(base_url('admin/menuitems?menu_id=' . $menu_id . '&res_id=' . $res_id . '&type=add')); ?>">
                                <button type="button" class="btn btn-lg btn-info waves-effect">Add Menu Item
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="card">
                        <div class="header">
                            <h2>
                                Items
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover agencyTable dataTable">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th> Item Name</th>
                                        <th> Item Price</th>
                                        <th> Item Category</th>
                                        <th> Item Description</th>
                                        <th> Item Photo</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    if (isset($list) && !empty($list)) {
                                        foreach ($list as $key => $dataObj) {
                                            ?>
                                            <tr>
                                                <td><?php echo $dataObj->item_id; ?></td>
                                                <td><?php echo $dataObj->item_name; ?></td>
                                                <td><?php echo number_format((float)$dataObj->item_price, 2, '.', '') . " " . $dataObj->item_price_currency; ?></td>
                                                <td><?php echo $dataObj->category_name; ?></td>

                                                <td><?php echo $dataObj->item_description; ?></td>
                                                <td>
                                                    <img src="<?php echo base_url(); ?><?php echo $dataObj->item_photo; ?>"
                                                         height="50" width="50"/></td>

                                                <td>


                                                    <a href="<?php echo htmlspecialchars(base_url('admin/menuitems?type=edit&res_id=' . $res_id . '&menu_id=' . $menu_id . '&id=' . $dataObj->item_id)); ?>">
                                                        <button type="button" class="btn btn-warning waves-effect">
                                                            Edit
                                                        </button>
                                                    </a>

                                                    <a href="<?php echo htmlspecialchars(base_url('admin/menuitems/deleteCategory?res_id=' . $res_id . '&menu_id=' . $menu_id . '&id=' . $dataObj->item_id)); ?>">
                                                        <button type="button" class="btn btn-danger waves-effect">
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>

<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/agency.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
        async defer></script>