<?php
if ($type == "add" || $type == "edit") {
    ?>


    <?php
} else {
    ?>
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="button-demo">
                        <div class="body">
                            <a href="<?php echo htmlspecialchars(base_url('admin/restaurents')); ?>">
                                <button type="button" class="btn btn-default waves-effect">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    <span>BACK</span>
                                </button>
                            </a>
                        </div>
                    </div>


                    <?php
                    if (isset($list) && !empty($list)) {
                        foreach ($list as $key => $catObj) {
                            ?>
                            <div class="card">
                                <div class="header">
                                    <h2 class="alert alert-info">
                                        <strong><?= $catObj->category_name ?></strong>
                                    </h2>
                                </div>
                                <?php if (isset($catObj->items) && !empty($catObj->items)): ?>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover agencyTable dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Item Name</th>
                                                    <th>Item Price</th>
                                                    <th>Item Category</th>
                                                    <th>Item Description</th>
                                                    <th>Item Photo</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                foreach ($catObj->items as $key => $dataObj) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $dataObj->item_id; ?></td>
                                                        <td><?php echo $dataObj->item_name; ?></td>
                                                        <td><?php echo $dataObj->item_price . " " . $dataObj->item_price_currency; ?></td>
                                                        <td><?php echo $dataObj->category_name; ?></td>

                                                        <td><?php echo $dataObj->item_description; ?></td>
                                                        <td>
                                                            <img src="<?php echo base_url(); ?><?php echo $dataObj->item_photo; ?>"
                                                                 height="50" width="50"/></td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </section>
    <?php
}
?>

<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/menucategory.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
        async defer></script>