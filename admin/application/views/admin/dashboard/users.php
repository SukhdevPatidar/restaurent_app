<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
<?php
if ($type == "add" || $type == "edit") {
  ?>

  <section class="content">
    <div class="container-fluid">

      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <div class="card">
            <div class="header">
              <h2> User </h2>
            </div>
            <div class="body">

              <form id="usersForm" method="POST"
              enctype="multipart/form-data"
              action="<?php echo htmlspecialchars(base_url('admin/users/addUser')); ?>">
              <input type="hidden" name="user_id"
              value="<?php
              echo ($type == 'edit') ? $data->user_id : ''; ?>"/>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="text" class="form-control" name="userName"
                  required
                  value="<?php echo ($type == 'edit') ? $data->user_first : ''; ?>">
                  <label class="form-label"> User Name</label>
                </div>
                <div class="help-info"></div>
              </div>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="text" class="form-control" name="userSurname"
                  required
                  value="<?php echo ($type == 'edit') ? $data->user_last : ''; ?>">
                  <label class="form-label">User Surname</label>
                </div>
                <div class="help-info"></div>
              </div>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="email" class="form-control" name="userEmail"
                  required
                  value="<?php echo ($type == 'edit') ? $data->user_mail : ''; ?>">
                  <label class="form-label">User Email</label>
                </div>
                <div class="help-info"></div>
              </div>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="number" class="form-control" name="userPhone"
                  required
                  value="<?php echo ($type == 'edit') ? $data->user_phone : ''; ?>">
                  <label class="form-label"> User Phone</label>
                </div>
                <div class="help-info"></div>
              </div>

              <div class="form-group form-float">
                <div class="form-line">
                  <input type="file" id="userPhoto" class="form-control" name="photo" />
                  <label class="form-label"> User Photo</label>
                </div>
                <div class="help-info"></div>
              </div>

              <label class="form-label"> Home Address</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="material-icons">home</i>
                </span>

                <div class="form-line">
                  <input type="text" class="form-control"  name="homeAddress" placeholder="home address"  value="<?php echo $data->userHomeAddress?$data->userHomeAddress->location_address:0; ?>">
                  <input type="hidden"  name="homeAddressLat" value="<?php echo $data->userHomeAddress ? $data->userHomeAddress->location_lat : 0; ?>">
                  <input type="hidden"  name="homeAddressLng" value="<?php echo $data->userHomeAddress ? $data->userHomeAddress->location_lng : 0; ?>">
                </div>
                <span class="input-group-addon edit_location">
                  <i class="material-icons" onclick="onMapLocation('home')">edit</i>
                </span>
              </div>

              <label class="form-label"> Office Address</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="material-icons">map</i>
                </span>
                <div class="form-line">
                  <input type="text" class="form-control"  name="officeAddress"  placeholder="office address"  value="<?php echo $data->userOfficeAddress?$data->userOfficeAddress->location_address:0; ?>">
                  <input type="hidden"  name="officeAddressLat" value="<?php echo $data->userOfficeAddress?$data->userOfficeAddress->location_lat:0; ?>">
                  <input type="hidden"  name="officeAddressLng" value="<?php echo $data->userOfficeAddress?$data->userOfficeAddress->location_lng:0; ?>">
                </div>
                <span class="input-group-addon edit_location">
                  <i class="material-icons" onclick="onMapLocation('office')">edit</i>
                </span>
              </div>


              <div class="form-group">
                <div class="form-line">
                  <select class="form-control show-tick" name="userCityId"
                  id="userCityList"
                  required>
                  <option value=''>Select City</option>
                  <?php
                  foreach ($cities as $value) {
                    if ($type == 'edit' && $value->city_id == $data->user_city_id) {
                      echo "<option value='$value->city_id' selected>$value->city_name</option>";
                    } else {
                      echo "<option value='$value->city_id'>$value->city_name</option>";
                    }
                  } ?>

                </select>
              </div>
              <div class="help-info"></div>
            </div>

            <div class="form-group">
              <?php
              $index = 0;
              foreach (userStatusList() as $key => $value) {
                if (($type == 'edit' && $data->user_status == $key) || ($type != 'edit' && $index == 0)) {
                  echo "<input type='radio' name='userStatus' value='$key' id='user$key' class='with-gap' checked>";
                } else {
                  echo "<input type='radio' name='userStatus' value='$key' id='user$key' class='with-gap'>";
                }
                echo "<label for='user$key'>$value</label>";
                $index++;
              } ?>
            </div>

            <!-- <div class="form-group form-float">
            <div class="form-line">
            <input type="hidden" class="form-control" name="userHomeLat" required
            value="<?php echo ($type == 'edit') ? $data->restaurent_latitude : ''; ?>">
            <input type="hidden" class="form-control" name="userHomeLng" required
            value="<?php echo ($type == 'edit') ? $data->restaurent_longitude : ''; ?>">
            <input type="text" class="form-control" name="userHomeAddress" required
            value="<?php echo ($type == 'edit') ? $data->restaurent_address : ''; ?>">
            <label class="form-label">Address</label>
          </div>
          <div class="help-info"></div>
        </div>



        <div class="form-group form-float">
        <div class="form-line">
        <input type="hidden" class="form-control" name="userOfficeLat" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_latitude : ''; ?>">
        <input type="hidden" class="form-control" name="userOfficeLng" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_longitude : ''; ?>">
        <input type="text" class="form-control" name="userOfficeAddress" required
        value="<?php echo ($type == 'edit') ? $data->restaurent_address : ''; ?>">
        <label class="form-label">Address</label>
      </div>
      <button type="button"  onclick="onMapLocation()">Address</button>

      <div class="help-info"></div>
    </div> -->



    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
  </form>
</div>
</div>
</div>
</div>
</div>
</section>

<?php
} else {
  ?>
  <section class="content">
    <div class="container-fluid">
      <!-- Basic Examples -->
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


          <?php
          $status = $this->session->flashdata('status');
          $message = $this->session->flashdata('message');
          if (isset($status)):
            if ($status && isset($message)):
              ?>
              <div class="alert alert-success">
                <?= $message ?>
              </div>
            <?php endif; ?>
            <?php
            if (!$status && isset($message)):
              ?>
              <div class="alert alert-warning">
                <?= $message ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>


          <!--                    <div class="body">-->
          <!--                        <div class="button-demo">-->
          <!--                            <a href="--><?php //echo htmlspecialchars(base_url('admin/users?type=add'));?><!--">-->
          <!--                                <button type="button" class="btn btn-info waves-effect">Add User-->
          <!--                                </button>-->
          <!--                            </a>-->
          <!--                        </div>-->
          <!--                    </div>-->


          <div class="card">
            <div class="header">
              <h2>
                Users
              </h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover usersTable dataTable">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Mobile</th>
                      <th>City</th>
                      <th>Registered At</th>
                      <th>Photo</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>

                  <?php
                  if (isset($users) && !empty($users)) {
                    foreach ($users as $user) {
                      ?>
                      <tr>
                        <td><?php echo $user->user_id; ?></td>
                        <td><?php echo $user->user_first; ?></td>
                        <td><?php echo $user->user_phone; ?></td>
                        <td><?php echo $user->user_city_name; ?></td>
                        <td><?php echo getDateFormat($user->user_created)." ".getTimeFormat($user->user_created); ?></td>
                        <td>
                            <img src="<?php echo base_url(); ?><?php echo $user->user_photo; ?>"
                                 height="50" width="50"/>
                        </td>
                        <td><?php echo userStatus($user->user_status); ?></td>
                        <td>
                          <a href="<?php echo htmlspecialchars(base_url('admin/users?type=edit&id=' . $user->user_id)); ?>">
                            <button type="button" class="btn btn-warning waves-effect">
                              Edit
                            </button>
                          </a>

                                                                            <a href="
                          <?php echo htmlspecialchars(base_url('admin/users/deleteUser?id=' . $user->user_id));?>
                                                                                <button type="button" class="btn btn-danger waves-effect">
                                                                                   delete
                                                                                  </button>
                                                                              </a>
                        </td>
                      </tr>
                      <?php
                    }
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- #END# Basic Examples -->
  </div>
</section>
<?php
}
?>

<?php
$this->load->view("admin/dashboard/modals/locationMap.php");
?>

<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/users.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
async defer></script>
