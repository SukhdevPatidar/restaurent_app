<?php
if ($type == "add" || $type == "edit") {
    ?>

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>Category Form</h2>
                        </div>
                        <div class="body">
                            <form id="agencyForm" method="POST"
                                  enctype="multipart/form-data"
                                  action="<?php echo htmlspecialchars(base_url('admin/rescategory/addCategory')); ?>">


                                <input type="hidden" name="catId"
                                       value="<?php echo ($type == 'edit') ? $data->category_id : ''; ?>"/>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="categoryName"
                                               minlength="3" required
                                               value="<?php echo ($type == 'edit') ? $data->category_name : ''; ?>">
                                        <label class="form-label">Category Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" id="rescatPhoto" class="form-control" name="photo" />
                                        <label class="form-label"> Category Icon</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
} else {
        ?>
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="body">
                        <div class="button-demo">
                            <a href="<?php echo htmlspecialchars(base_url('admin/rescategory?type=add')); ?>">
                                <button type="button" class="btn btn-info waves-effect">Add Category
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="card">
                        <div class="header">
                            <h2>
                                Categories
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover agencyTable dataTable">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Category Name</th>
                                        <th>Category Icon</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    if (isset($list) && !empty($list)) {
                                        foreach ($list as $key => $dataObj) {
                                            ?>
                                            <tr>
                                                <td><?php echo $dataObj->category_id; ?></td>
                                                <td><?php echo $dataObj->category_name; ?></td>
                                                <td>
                                                    <img src="<?php echo base_url(); ?><?php echo $dataObj->category_icon_path; ?>"
                                                         height="50" width="50"/>
                                                </td>

                                                <td>
                                                    <a href="<?php echo htmlspecialchars(base_url('admin/rescategory?type=edit&id=' . $dataObj->category_id)); ?>">
                                                        <button type="button" class="btn btn-warning waves-effect">
                                                            Edit
                                                        </button>
                                                    </a>

                                                    <a href="<?php echo htmlspecialchars(base_url('admin/rescategory/deleteCategory?id=' . $dataObj->category_id)); ?>">
                                                        <button type="button" class="btn btn-danger waves-effect">
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    }
?>

<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/agency.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
        async defer></script>
