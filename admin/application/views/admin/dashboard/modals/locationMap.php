<!-- Bootstrap Select Css -->
<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>

<div class="modal fade" id="locationMapModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <h4 class="modal-title" id="defaultModalLabel">Delivery</h4>-->
            </div>
            <form id="order_form" method="POST"
                  action="<?php echo htmlspecialchars(base_url('admin/smartshop/updateOrder')); ?>">
                <input type="hidden" name="orderId">

                <div class="modal-body">
                  <div class="form-group">
                      <div id="locationMap" style="width: 100%; height: 300px"
                           latitude="0"
                           longitude="0"></div>
                  </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="submit" class="btn btn-link waves-effect">SAVE</button> -->
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>
