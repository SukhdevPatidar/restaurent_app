<div class="modal fade" id="commissionAddModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Commission Form</h4>
            </div>
            <form id="commission_form" method="POST"
                  action="<?php echo htmlspecialchars(base_url('admin/commission/addCommission')); ?>">

                <div class="modal-body">
                    <div class="body">

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control" name="price" required>
                                <label class="form-label">Delivery Price</label>
                            </div>
                        </div>
                        <div class="form-group">App Type</div>
                        <div class="form-group" id="appType">
                            <?php
                            $index = 0;
                            foreach (appTypeList() as $key => $value) {
                                if ($index == 0) {
                                    echo "<input type='radio' name='appType' value='$key' id='appType$key' class='with-gap' checked>
                                        <label for='appType$key'>$value</label>";
                                } else {
                                    echo "<input type='radio' name='appType' value='$key' id='appType$key' class='with-gap'>
                                        <label for='appType$key'>$value</label>";
                                }
                                $index++;
                            }
                            ?>
                        </div>
                        <div class="form-group" id="vehicleLabel">Delivery Vehicle</div>
                        <div class="form-group" id="vehicle">
                            <?php
                            $index = 0;
                            foreach (vehicleTypeList() as $key => $value) {
                                if ($index == 0) {
                                    echo "<input type='radio' name='vehicle' value='$key' id='vehicle$key' class='with-gap' checked>
                                        <label for='vehicle$key'>$value</label>";
                                } else {
                                    echo "<input type='radio' name='vehicle' value='$key' id='vehicle$key' class='with-gap'>
                                        <label for='vehicle$key'>$value</label>";
                                }
                                $index++;
                            }
                            ?>
                        </div>
                        <div class="form-group" id="speedLabel">Delivery Speed</div>
                        <div class="form-group" id="speed">
                            <?php
                            $index = 0;
                            foreach (deliverySpeedTypeList() as $key => $value) {
                                if ($index == 0) {
                                    echo "<input type='radio' name='speed' value='$key' id='speed$key' class='with-gap' checked>
                                        <label for='speed$key'>$value</label>";
                                } else {
                                    echo "<input type='radio' name='speed' value='$key' id='speed$key' class='with-gap'>
                                        <label for='speed$key'>$value</label>";
                                }
                                $index++;
                            }
                            ?>
                        </div>
                        <div class="form-group" id="areaLabel">Delivery Area</div>
                        <div class="form-group" id="area">
                            <?php
                            $index = 0;
                            foreach (deliveryAreaTypeList() as $key => $value) {
                                if ($index == 0) {
                                    echo "<input type='radio' name='area' value='$key' id='area$key' class='with-gap' checked>
                                        <label for='area$key'>$value</label>";
                                } else {
                                    echo "<input type='radio' name='area' value='$key' id='area$key' class='with-gap'>
                                        <label for='area$key'>$value</label>";
                                }
                                $index++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>