<!-- Bootstrap Select Css -->
<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>

<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <h4 class="modal-title" id="defaultModalLabel">Delivery</h4>-->
            </div>
            <form id="delivery_form" method="POST"
                  action="<?php echo htmlspecialchars(base_url('admin/delivery/updateDelivery')); ?>">
                <input type="hidden" name="deliveryId">
                <input type="hidden" name="deliveryType">

                <div class="modal-body">
                    <div class="body">
                        <div class="row clearfix">
                            <p>
                                <b>Delivery Status</b>
                            </p>
                            <select class="form-control show-tick" name="deliveryStatus">
                                <?php
                                foreach (deliveryStatusList() as $key => $value) {
                                    echo "<option value='$key'>$value</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>