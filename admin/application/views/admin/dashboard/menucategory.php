<script src="http://rubaxa.github.io/Sortable/Sortable.js"></script>

<?php
if ($type == "add" || $type == "edit") {
    ?>

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>Menu Category </h2>
                        </div>
                        <div class="body">
                            <form id="menuCategoryForm" method="POST"
                                  action="<?php echo htmlspecialchars(base_url('admin/menucategory/addCategory')); ?>">

                                <input type="hidden" name="res_id"
                                       value="<?= $res_id ?>"/>
                                <input type="hidden" name="catId"
                                       value="<?php echo ($type == 'edit') ? $data->category_id : ''; ?>"/>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="categoryName"
                                               minlength="3" required
                                               value="<?php echo ($type == 'edit') ? $data->category_name : ''; ?>">
                                        <label class="form-label"> Menu Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
} else {
    ?>
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="body">
                        <div class="button-demo">
                            <a href="<?php echo htmlspecialchars(base_url('admin/restaurents')); ?>">
                                <button type="button" class="btn btn-default waves-effect">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    <span>BACK</span>
                                </button>
                            </a>

                            <a href="<?php echo htmlspecialchars(base_url('admin/menucategory?res_id=' . $res_id . '&type=add')); ?>">
                                <button type="button" class="btn btn-lg btn-info waves-effect">Add Menu Category
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="card">
                        <div class="header">
                            <h2>
                                Categories
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover menuCategoryTable dataTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Id</th>
                                        <th>Menu Category Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="itemTableBody">
                                    <?php
                                    if (isset($list) && !empty($list)) {
                                        foreach ($list as $key => $dataObj) {
                                            ?>
                                            <tr categoryId="<?php echo $dataObj->category_id; ?>"
                                                resId="<?php echo $res_id; ?>">
                                                <td class="drag-handler"
                                                    categoryId="<?php echo $dataObj->category_id; ?>"
                                                    resId="<?php echo $res_id; ?>">
                                                    <i class="material-icons">swap_vert</i>
                                                </td>
                                                <td><?php echo $dataObj->category_id; ?></td>
                                                <td><?php echo $dataObj->category_name . " (" . $dataObj->items_count . ")"; ?></td>
                                                <td>
                                                    <a href="<?php echo htmlspecialchars(base_url('admin/menuitems?res_id=' . $res_id . '&menu_id=' . $dataObj->category_id)); ?>">
                                                        <button type="button" class="btn btn-primary waves-effect">
                                                            Items
                                                        </button>
                                                    </a>

                                                    <a href="<?php echo htmlspecialchars(base_url('admin/menucategory?type=edit&res_id=' . $res_id . '&id=' . $dataObj->category_id)); ?>">
                                                        <button type="button" class="btn btn-warning waves-effect">
                                                            Edit
                                                        </button>
                                                    </a>

                                                    <a href="<?php echo htmlspecialchars(base_url('admin/menucategory/deleteCategory?res_id=' . $res_id . '&id=' . $dataObj->category_id)); ?>">
                                                        <button type="button" class="btn btn-danger waves-effect">
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php
}
?>


<script src="<?php echo base_url(); ?>assets/admin/js/googlemap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/dashboard/menucategory.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRwNwnQ2mOSTe-hPCXFl_MSu373CjdOM&libraries=places&callback=initMap"
        async defer></script>