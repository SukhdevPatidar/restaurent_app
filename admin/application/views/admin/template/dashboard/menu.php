<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?php echo base_url(); ?>assets/admin/images/user.png" width="48" height="48" alt="User"/>
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php
                    $user = $this->session->userdata('user_data');
                    echo $user->user_first . " " . $user->user_last;
                    ?>
                </div>

                <div class="email">
                    <?php
                    echo $user->user_mail;
                    ?>
                </div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <!--                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>-->
                        <!--                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>-->
                        <!--                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>-->
                        <!--                        <li role="separator" class="divider"></li>-->
                        <li><a href="<?php echo htmlspecialchars(base_url('admin/logout')); ?>"><i
                                        class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li style="display: <?php echo sideMenuItemVisibility('home'); ?>;">
                    <a href="<?php echo htmlspecialchars(base_url('admin/rescategory')); ?>">
                        <i class="material-icons">home</i>
                        <span>Restaurent Categories</span>
                    </a>
                </li>
                <li style="display: <?php echo sideMenuItemVisibility('users'); ?>;">
                    <a href="<?php echo htmlspecialchars(base_url('admin/cities')); ?>">
                        <i class="material-icons">group</i>
                        <span>Cities</span>
                    </a>
                </li>
                <li style="display: <?php echo sideMenuItemVisibility('send'); ?>;">
                    <a href="<?php echo htmlspecialchars(base_url('admin/restaurents')); ?>">
                        <i class="material-icons">send</i>
                        <span>Restaurents</span>
                    </a>
                </li>
                <li style="display: <?php echo sideMenuItemVisibility('sell'); ?>;">
                    <a href="<?php echo htmlspecialchars(base_url('admin/users')); ?>">
                        <i class="material-icons">people</i>
                        <span>Users</span>
                    </a>
                </li>
                <?php
                /*                <li style="display: <?php echo sideMenuItemVisibility('commission');?>;">*/
                /*                    <a href="<?php echo htmlspecialchars(base_url('admin/commission')); ?>">*/
                //                        <i class="material-icons">layers</i>
                //                        <span>Commissions</span>
                //                    </a>
                //                </li>
                /*                <li style="display: <?php echo sideMenuItemVisibility('agency');?>;">*/
                /*                    <a href="<?php echo htmlspecialchars(base_url('admin/agency')); ?>">*/
                //                        <i class="material-icons">contacts</i>
                //                        <span>Agencies</span>
                //                    </a>
                //                </li>
                /*                <li style="display: <?php echo sideMenuItemVisibility('employee');?>;">*/
                /*                    <a href="<?php echo htmlspecialchars(base_url('admin/employee')); ?>">*/
                //                        <i class="material-icons">contacts</i>
                //                        <span>Employees</span>
                //                    </a>
                //                </li>
                /*                <li style="display: <?php echo sideMenuItemVisibility('smartshop');?>;">*/
                /*                    <a href="<?php echo htmlspecialchars(base_url('admin/smartshop')); ?>">*/
                //                        <i class="material-icons">location_city</i>
                //                        <span>Smartshops</span>
                //                    </a>
                //                </li>
                /*                <li style="display: <?php echo sideMenuItemVisibility('payment');?>;">*/
                /*                    <a href="<?php echo htmlspecialchars(base_url('admin/payment')); ?>">*/
                //                        <i class="material-icons">payment</i>
                //                        <span>Payments</span>
                //                    </a>
                //                </li>
                ?>
            </ul>
        </div>
    </aside>
    <!-- #END# Right Sidebar -->
</section>