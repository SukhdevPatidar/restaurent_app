﻿<body class="fp-page">
<div class="fp-box">
    <?php $this->load->view('admin/template/auth/logo'); ?>

    <div class="card">
        <div class="body">
            <form id="forgot_password" method="POST" action="<?php echo htmlspecialchars(base_url('admin/forgotPassword/reset')); ?>">
                <div class="msg">
                    Enter your email address that you used to register. We'll send you an email with your username and a
                    link to reset your password.
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                    </div>
                </div>

                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD</button>

                <div class="row m-t-20 m-b--5 align-center">
                    <a href="signin">Sign In!</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<!-- Custom Js -->
<script src="<?php echo base_url(); ?>assets/admin/js/pages/auth/forgot-password.js"></script>
