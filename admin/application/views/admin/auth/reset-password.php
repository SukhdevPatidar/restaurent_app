<body class="fp-page">
<div class="fp-box">

    <?php if (isset($success)) { ?>
        <p style="width:100%;text-align: center; font-size: 30px; color: greenyellow"> <?php echo $success; ?></p>
    <?php } else if (isset($error)) { ?>
        <p style="width:100%;text-align: center; font-size: 30px; color: red"> <?php echo $error; ?></p>
    <?php } else {
        if (isset($resetId)) { ?>
            <?php $this->load->view('admin/template/auth/logo'); ?>

            <div class="card">
                <div class="body">
                    <form id="reset_password"
                          action="<?php echo htmlspecialchars(base_url('resetPassword/updatedPassword')); ?>"
                          method="post">
                        <input type="hidden" name="resetId" class="form-control" placeholder="Password"
                               value="<?php echo $resetId; ?>">

                        <div class="msg">
                            Enter new password to reset your old password.
                        </div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="New Password"
                                       required autofocus>
                            </div>

                        </div>

                        <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD
                        </button>
                    </form>
                </div>
            </div>
        <?php }
    } ?>
</div>
</body>
