var myLatLng = {
    lat: 25.1984137,
    lng: 55.23835829999996
};
var map;
var geocoder;
var marker;
var placeSearchInput;

var addressCallback;

function setUserLocation(lat, lng) {
    lat = parseFloat(lat);
    lng = parseFloat(lng);
    myLatLng.lat = lat;
    myLatLng.lng = lng;
    map.setCenter(myLatLng);
    reverseGeocode(lat, lng);
}

function initMap() {
    var mapElem = document.getElementById('locationMap');

    // Set location to map
    var lat = mapElem.getAttribute("latitude");
    var lng = mapElem.getAttribute("longitude");

    if (lat && lng) {
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        myLatLng.lat = lat;
        myLatLng.lng = lng;
        reverseGeocode(lat, lng);
    } else {
        navigator.geolocation.getCurrentPosition(function (location) {
            myLatLng = {
                lat: location.coords.latitude,
                lng: location.coords.longitude
            };
            map.setCenter(myLatLng);
            reverseGeocode(myLatLng.lat, myLatLng.lng);
        }, function (err) {
            reverseGeocode(myLatLng.lat, myLatLng.lng);
        });
    }

    map = new google.maps.Map(mapElem, {
        center: myLatLng,
        zoom: 15,
        panControl: true,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        rotateControl: false,
        fullscreenControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Create the search box and link it to the UI element.
    placeSearchInput = document.createElement("input");
    placeSearchInput.style.position = "absolute";
    placeSearchInput.style.left = "8px";
    placeSearchInput.style.top = "8px";
    placeSearchInput.style.maxWidth = "300px";
    placeSearchInput.style.width = "calc(100% - 16px)";
    placeSearchInput.style.height = "40px";
    placeSearchInput.style.padding = "5px";
    placeSearchInput.placeholder = "Search Location";
    mapElem.appendChild(placeSearchInput);
    var autocomplete = new google.maps.places.Autocomplete(placeSearchInput);

    autocomplete.bindTo('bounds', this.map);

    autocomplete.addListener('place_changed', (event) => {
        var places = autocomplete.getPlace();
        if (!places.geometry) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (places.geometry.viewport) {
            this.map.fitBounds(places.geometry.viewport);
        } else {
            this.map.setCenter(places.geometry.location);
            this.map.setZoom(17); // Why 17? Because it looks good.
        }
        marker.setPosition(places.geometry.location);
        fireCallback(places.geometry.location.lat(), places.geometry.location.lng(), places.formatted_address);
        /* window.alert("Address: '" + place.formatted_address + "'");*/
    });

    adMarker();
    google.maps.event.addListener(map, 'click', function (event) {
        marker.setPosition({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
        });
        reverseGeocode(event.latLng.lat(), event.latLng.lng());
    });
}

function fireCallback(lat, lng, address = "") {
    if (addressCallback) {
        addressCallback(lat, lng, address);
    }
}


function adMarker() {
    marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        draggable: true,
        title: ''
    });
    google.maps.event.addListener(marker, 'dragend', function (event) {
        reverseGeocode(event.latLng.lat(), event.latLng.lng());
    });

}


function reverseGeocode(Latitude, Longitude, getAddress) {
    geocoder = new google.maps.Geocoder;
    var latlng = {
        lat: Latitude,
        lng: Longitude
    };
    geocoder.geocode({
        'location': latlng
    }, (results, status) => {
        if (status === 'OK') {
            if (results[0]) {
                placeSearchInput.value = results[0].formatted_address;
                fireCallback(Latitude, Longitude, results[0].formatted_address);
            } else {
                console.log('No results found');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
    });


}
