$(function () {


    $('#commission_form').validate({
        rules: {
            'vehicle': {
                required: true
            },
            'speed': {
                required: true
            },
            'area': {
                required: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });


    $('input[type=radio][name=appType]').change(function () {
        if (this.value == 0) {
           showNormalAppForm();
        }
        else if (this.value == 1) {
           showSmartshopAppForm();
        }
    });

});

function onAddCommision() {
    showNormalAppForm();
    $('#commission_form').trigger("reset");
    $("#commissionAddModal").modal('toggle');

    setTimeout(function () {
        var price = $("#commission_form input[name=price]");
        price.focus();
    }, 500);
}

function showNormalAppForm() {
    $("#vehicle").show();
    $("#area").show();
    $("#speed").show();
    $("#vehicleLabel").show();
    $("#areaLabel").show();
    $("#speedLabel").show();
}

function showSmartshopAppForm() {
    $("#vehicle").hide();
    $("#area").hide();
    $("#speed").hide();
    $("#vehicleLabel").hide();
    $("#areaLabel").hide();
    $("#speedLabel").hide();
}

function onEdit(data) {
    var commision = JSON.parse(data);

    var appType = $("#commission_form input[name=appType]");
    var price = $("#commission_form input[name=price]");
    var vehicle = $("#commission_form input[name=vehicle]");
    var speed = $("#commission_form input[name=speed]");
    var area = $("#commission_form input[name=area]");

    price.val(commision.delivery_price);
    setTimeout(function () {
        price.focus();
    }, 500);


    if (commision.delivery_app_type == 0) {
        showNormalAppForm();
    }
    else if (commision.delivery_app_type == 1) {
        showSmartshopAppForm();
    }

    for (var i = 0; i < appType.length; i++) {
        var elem = appType[i];
        if (elem.value == commision.delivery_app_type) {
            elem.checked = true;
            break;
        }
    }

    for (var i = 0; i < vehicle.length; i++) {
        var elem = vehicle[i];
        if (elem.value == commision.deivery_price_vehicle) {
            elem.checked = true;
            break;
        }
    }

    for (var i = 0; i < speed.length; i++) {
        var elem = speed[i];
        if (elem.value == commision.delivery_price_service_speed) {
            elem.checked = true;
            break;
        }
    }

    for (var i = 0; i < area.length; i++) {
        var elem = area[i];
        if (elem.value == commision.delivery_price_city_area) {
            elem.checked = true;
            break;
        }
    }
    $("#commissionAddModal").modal('toggle');
}