$(function () {

    $('.agencyTable').DataTable({
        responsive: true
    });

    //Advanced Form Validation
    $('#agencyForm').validate({
        rules: {
            'date': {
                customdate: true
            },
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });




});

var photoField = $("#resPhoto");
var rescatPhoto = $("#rescatPhoto");
setTimeout(function () {
    photoField.focus();
    rescatPhoto.focus();
}, 1000);

addressCallback = function (lat, lng, address) {
    var addressField = $("#agencyForm input[name=restaurentAddress]");
    var agencyLatField = $("#agencyForm input[name=restaurentLat]");
    var agencyLngField = $("#agencyForm input[name=restaurentLng]");
    addressField.val(address);
    agencyLatField.val(lat);
    agencyLngField.val(lng);
    setTimeout(function () {
        addressField.focus();
    }, 100)
};
