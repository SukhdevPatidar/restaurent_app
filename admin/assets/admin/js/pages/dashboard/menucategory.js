$(function () {
    //
    // $('.menuCategoryTable').DataTable({
    //     responsive: true
    // });

    //Advanced Form Validation
    $('#menuCategoryForm').validate({
        rules: {
            'date': {
                customdate: true
            },
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

});

function updateOrder(data) {
    $.ajax({
        url: 'http://neemuchmandirate.com/sdev/restaurents/admin/menucategory/updateOrder',
        type: "POST",
        data: data,
        success: function (res) {
            // var res = JSON.parse(res);
            console.log(res);
        },
        error: function (f) {
            console.log("Error " + JSON.stringify(f));
        }
    })
}

Sortable.create(
    $('#itemTableBody')[0], {
        animation: 150,
        scroll: true,
        handle: '.drag-handler',
        onEnd: function (evt) {
            var itemEl = evt.item;  // dragged HTMLElement
            evt.to;    // target list
            evt.from;  // previous list
            evt.oldIndex;  // element's old index within old parent
            evt.newIndex;  // element's new index within new parent
            var allTR = evt.from.querySelectorAll("tr");
            var allCats = [];

            allTR.forEach((val) => {
                allCats.push(val.getAttribute("categoryId"));
            });

            var data = {
                categories: allCats,
                resId: itemEl.getAttribute("resId"),
            };
            updateOrder(data);
        }
    }
);