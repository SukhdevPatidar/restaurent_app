$(function() {

	$('.usersTable').DataTable({
		responsive: true
	});


	//Advanced Form Validation
	$('#usersForm').validate({
		rules: {
			'date': {
				customdate: true
			},
		},
		highlight: function(input) {
			$(input).parents('.form-line').addClass('error');
		},
		unhighlight: function(input) {
			$(input).parents('.form-line').removeClass('error');
		},
		errorPlacement: function(error, element) {
			$(element).parents('.form-group').append(error);
		}
	});

});

var userPhoto = $("#userPhoto");
setTimeout(function () {
    userPhoto.focus();
}, 1000);

var addressElem, addressLatElem, addressLngElem, addressType;

function onMapLocation(locationType) {
	addressType = locationType;
	if (locationType == "home") {
		addressElem = $("#usersForm input[name=homeAddress]");
		addressLatElem = $("#usersForm input[name=homeAddressLat]");
		addressLngElem = $("#usersForm input[name=homeAddressLng]");
	}

	if (locationType == "office") {
		addressElem = $("#usersForm input[name=officeAddress]");
		addressLatElem = $("#usersForm input[name=officeAddressLat]");
		addressLngElem = $("#usersForm input[name=officeAddressLng]");
	}

	$('#map_form').trigger("reset");
	var mapElem = document.getElementById('locationMap');

	$("#locationMapModal").modal('toggle');

	setTimeout(function() {
		setUserLocation(addressLatElem.val(), addressLngElem.val());
	}, 500);
}

addressCallback = function(lat, lng, address) {
	addressElem.attr('value', address);
	addressLatElem.attr('value', lat);
	addressLngElem.attr('value', lng);

	addressElem.val(address);
	addressLatElem.val(lat);
	addressLngElem.val(lng);
	setTimeout(function() {
		addressElem.focus();
	}, 500);
};
