$(function () {

    $('.sellsTable').DataTable({
        responsive: true
    });

    $('.sendsTable').DataTable({
        responsive: true
    });


});


function onEdit(data) {
    var dataObj = JSON.parse(data);

    var deliveryId = $("#delivery_form input[name=deliveryId]");
    var deliveryType = $("#delivery_form input[name=deliveryType]");
    var deliveryStatus = $("#delivery_form select[name=deliveryStatus]");

    deliveryType.val(dataObj.delivery_type);
    deliveryId.val(dataObj.delivery_id);
    deliveryStatus.val(dataObj.delivery_status).change();

    $("#deliveryModal").modal('toggle');
}