$(function () {

    $('.smartshopTable').DataTable({
        responsive: true
    });

    //Advanced Form Validation
    $('#smartshopForm').validate({
        rules: {
            'date': {
                customdate: true
            },
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    var baseUrl = window.location.origin;
    var mainDir = window.location.pathname.split('/')[1];
    var apiBaseUrl = baseUrl + "/" + mainDir;

    $('#countryList').change(function () {
        emptyDropdown($("#statesList"));
        emptyDropdown($("#citiesList"));
        $.ajax({
            type: "GET",
            url: apiBaseUrl + "/getStates?country_id=" + this.value,
            cache: false,
            data: "",
            success: function (res) {
                if (res.status) {
                    let list = res.data;
                    addOptionsToDropdown("statesList", list)
                }
            },
        });
    });

    $('#statesList').change(function () {
        emptyDropdown($("#citiesList"));

        $.ajax({
            type: "GET",
            url: apiBaseUrl + "/getCities?state_id=" + this.value,
            cache: false,
            data: "",
            success: function (res) {
                if (res.status) {
                    let list = res.data;
                    addOptionsToDropdown("citiesList", list)
                }
            },
        });
    });
});

function emptyDropdown(element) {
    element.find('option:not(:first)').remove();
    element.selectpicker("refresh");
}
function addOptionsToDropdown(elemId, list, key = "id", value = "name") {
    var element = $("#" + elemId);
    emptyDropdown(element);
    list.map((obj) => {
        element.append("<option value='" + obj[key] + "'>" + obj[value] + "</option>");
    });
    element.selectpicker("refresh");
}


addressCallback = function (lat, lng, address) {
    var addressField = $("#agencyForm input[name=smartshopAddress]");
    var latField = $("#agencyForm input[name=smartshopLat]");
    var lngField = $("#agencyForm input[name=smartshopLng]");
    addressField.val(address);
    latField.val(lat);
    lngField.val(lng);
    setTimeout(function () {
        addressField.focus();
    }, 100)
};