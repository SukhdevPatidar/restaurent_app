$(function () {

    $('.smartshopOrderTable').DataTable({
        responsive: true
    });

});


function onEdit(data) {
    var dataObj = JSON.parse(data);

    var orderId = $("#order_form input[name=orderId]");
    var orderStatus = $("#order_form select[name=orderStatus]");

    // deliveryType.val(dataObj.delivery_type);
    orderId.val(dataObj.smartshop_order_id);
    orderStatus.val(dataObj.smartshop_order_status).change();

    $("#orderModal").modal('toggle');
}