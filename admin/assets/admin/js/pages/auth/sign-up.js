$(function () {
    $('#sign_up').validate({
        rules: {
            'terms': {
                required: true
            },
            'confirm': {
                equalTo: '[name="password"]'
            }
        },
        highlight: function (input) {
            console.log(input);
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
            $(element).parents('.form-group').append(error);
        },
        submitHandler: function (form) {
            form.submit();

            // var userName = form.elements.email.value;
            // var password = form.elements.password.value;
            // $.ajax({
            //     url: 'http://smartpoint.webmobilyazilim.com/api/admin/register/signup',
            //     type: "POST",
            //     data: $("#sign_up").serialize(),
            //     success: function (res) {
            //
            //         var res = JSON.parse(res);
            //         if (res.status) {
            //         } else {
            //             alert(res.message);
            //         }
            //     },
            //     error: function (f) {
            //         console.log("Error " + JSON.stringify(f));
            //     }
            // })
        }
    });
});