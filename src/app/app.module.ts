import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import {Camera} from '@ionic-native/camera';
import {LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator';
import {LocationAccuracy} from '@ionic-native/location-accuracy';
import {GoogleAnalytics} from '@ionic-native/google-analytics';
import {AppVersion} from '@ionic-native/app-version';
import {Firebase} from '@ionic-native/firebase';
import {AngularFireModule} from "angularfire2";
import {AngularFireAuth} from 'angularfire2/auth';
import {Geolocation} from '@ionic-native/geolocation';

import {MyApp} from './app.component';

import {CategoriesPageModule} from '../pages/categories/categories.module';
import {HomePage} from '../pages/home/home';
import {RestaurentmenuPageModule} from '../pages/restaurentmenu/restaurentmenu.module';
import {ProfilePageModule} from '../pages/profile/profile.module';
import {OtpverifyPageModule} from '../pages/otpverify/otpverify.module';
import {RestaurentsPageModule} from '../pages/restaurents/restaurents.module';
import {UserlocationPageModule} from '../pages/userlocation/userlocation.module';
import {SidemenuPageModule} from '../pages/sidemenu/sidemenu.module';
import {PhonecodePageModule} from '../pages/phonecode/phonecode.module';
import {SettingPageModule} from '../pages/setting/setting.module';
import {UserpositionsPageModule} from '../pages/userpositions/userpositions.module';
import {LocationmapPageModule} from '../pages/locationmap/locationmap.module';
import {AutocompletePageModule} from '../pages/autocomplete/autocomplete.module';
import {DriversMapPageModule} from '../pages/drivers-map/drivers-map.module';

import {NavheaderComponent} from '../components/navheader/navheader';

import {PipesModule} from '../pipes/pipes.module';

import {ApiProvider} from '../providers/api/api';
import {UtilProvider} from '../providers/util/util';
import {FirebaseProvider} from '../providers/firebase/firebase';
import {CallNumber} from '@ionic-native/call-number';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        NavheaderComponent,
    ],
    imports: [
        CategoriesPageModule,
        RestaurentmenuPageModule,
        ProfilePageModule,
        OtpverifyPageModule,
        RestaurentsPageModule,
        UserlocationPageModule,
        SidemenuPageModule,
        PhonecodePageModule,
        SettingPageModule,
        UserpositionsPageModule,
        LocationmapPageModule,
        AutocompletePageModule,
        DriversMapPageModule,
        PipesModule,
        HttpModule,
        AngularFireModule.initializeApp(FirebaseProvider.config),
        BrowserModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage
    ],
    providers: [
        Geolocation,
        AngularFireAuth,
        Firebase,
        StatusBar,
        SplashScreen,
        LocationAccuracy,
        Camera,
        LaunchNavigator,
        GoogleAnalytics,
        AppVersion,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ApiProvider,
        UtilProvider,
        FirebaseProvider,
        CallNumber
    ]
})
export class AppModule {
}
