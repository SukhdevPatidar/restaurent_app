import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, Config, Events} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {SidemenuPage} from '../pages/sidemenu/sidemenu';
import {CategoriesPage} from '../pages/categories/categories';
import {ApiProvider} from '../providers/api/api';
import {Firebase} from '@ionic-native/firebase';
import {OtpverifyPage} from '../pages/otpverify/otpverify';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

import {HomePage} from '../pages/home/home';
import {AppVersion} from '@ionic-native/app-version';
import {FirebaseProvider} from "../providers/firebase/firebase";

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage;

    constructor(
        private appVersion: AppVersion,
        private ga: GoogleAnalytics,
        public platform: Platform,
        statusBar: StatusBar,
        public apiProvider: ApiProvider,
        splashScreen: SplashScreen,
        private config: Config,
        private firebase: Firebase,
        private events: Events,
        private firebaseProvider: FirebaseProvider,
    ) {
        platform.ready().then(() => {
            this.startGoogleAnalytics();

            this.config.set('backButtonIcon', 'ios-arrow-back-outline');
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();


            // this.firebaseProvider.initializeFirebase();

            if (this.apiProvider.isDevice()) {
                if (this.platform.is('ios')) {
                    this.firebase.hasPermission().then(({isEnabled}) => {
                        if (!isEnabled) {
                            this.firebase.grantPermission();
                        }
                    });
                }
                this.firebase.setBadgeNumber(0);

                this.firebase.getToken()
                    .then(token => {
                        console.log(`Got a new token ${token}`);
                        localStorage.setItem("push_token", token);
                        this.updatePushToken();
                    }) // save the token server-side and use it to push notifications to this device
                    .catch(error => console.error('Error getting token', error));

                this.firebase.onTokenRefresh()
                    .subscribe((token: string) => {
                        localStorage.setItem("push_token", token);
                        console.log(`Got a new token ${token}`);
                        this.updatePushToken();
                    });

                this.firebase.onNotificationOpen().subscribe((notification) => {
                    let data = notification.data;
                    let type = data.type;
                    let driver_request_id = data.driver_request_id;
                    let sender_id = data.sender_id;
                    this.events.publish("UpdateRestaurentLocations");

                    if (!notification.tap) {

                    } else {
                        this.firebase.setBadgeNumber(0);
                    }
                });
            }
        });
    }

    updatePushToken = () => {
        let pushToken = localStorage.getItem("push_token");
        if (pushToken) {
            this.updateProfilePushToken(pushToken);
            this.events.subscribe("LOGGED_IN", () => {
                let pushToken = localStorage.getItem("push_token");
                this.updateProfilePushToken(pushToken);
            });
        }
    }

    updateProfilePushToken(pushToken) {
        var user = this.apiProvider.getUser();
        if (user && pushToken) {
            var userData = {
                'user_id': user.user_id,
                'push_token': pushToken,
            };
            this.apiProvider.postRequest(ApiProvider.METHODS.updateProfile, userData).then((res: any) => {
                    if (res.status) {
                        this.apiProvider.setUser(res.data);
                    }
                },
                (error) => {
                });
        }
    }


    startGoogleAnalytics() {
        this.ga.startTrackerWithId(this.apiProvider.G_TRACKING_ID)
            .then(() => {
                if (this.apiProvider.getUser()) {
                    this.ga.setUserId(this.apiProvider.getUser().user_id);
                }

                this.appVersion.getVersionNumber().then((versionNumber) => {
                    this.ga.setAppVersion(versionNumber);
                });

                console.log('Google analytics is ready now');
                this.ga.trackView('test');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
    }
}
