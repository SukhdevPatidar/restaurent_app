import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { CategoriesPage } from '../../pages/categories/categories';

/**
 * Generated class for the NavheaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'navheader',
  templateUrl: 'navheader.html',
  inputs: ['title']
})
export class NavheaderComponent {
  @ViewChild(Nav) nav: Nav;

  title = "";

  constructor() {
    console.log('Hello NavheaderComponent Component');
  }

  home() {
    this.nav.setRoot(CategoriesPage);
  }

}
