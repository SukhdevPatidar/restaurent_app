import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";

import { ImagePathPipe } from './image-path/image-path';
@NgModule({
	declarations: [ImagePathPipe],
	imports: [CommonModule],
	providers: [ImagePathPipe],
	exports: [ImagePathPipe]
})
export class PipesModule {}
