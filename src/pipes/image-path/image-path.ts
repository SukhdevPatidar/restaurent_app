import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ImagePathPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'imagePath',
})
export class ImagePathPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {

    if (args && args[0] && !value) {
      let defaultImage = args[0];
      return defaultImage;
    }
    if(value) {
      return "http://gommistaeconomy.it/velofood/" + value.toLowerCase();
    }
    return "";
  }
}
