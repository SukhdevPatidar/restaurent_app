import {Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the AutocompletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
    selector: 'page-autocomplete',
    templateUrl: 'autocomplete.html',
})
export class AutocompletePage {
    @ViewChild('search') searchElement: ElementRef;
    map;
    callBack;
    myLocations;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this.map = navParams.get('map');
        this.callBack = navParams.get('callBack');
        this.myLocations = navParams.get('myLocations');
    }

    ionViewDidLoad() {
        var searchElem: any = this.searchElement;
        let placeName = this.navParams.get('placeName');
        searchElem.value = placeName;
        setTimeout(() => {
            searchElem.setFocus();
        }, 500);
        setTimeout(() => {
            searchElem.setFocus();
        }, 1000);


        console.log('ionViewDidLoad AutocompletePage');

        //Autocomplete
        var input = document.querySelector('#search input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', this.map);

        autocomplete.addListener('place_changed', (event) => {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                console.log("No details available for input: '" + place.name + "'");
                return;
            }
            if (this.callBack) {
                let location = place.geometry.location;
                this.callBack(location.lat(), location.lng(), place.formatted_address);
                this.cancelClick();
            }

            // if (place.geometry.viewport) {
            //   this.map.fitBounds(place.geometry.viewport);
            // } else {
            //   this.map.setCenter(place.geometry.location);
            //   this.map.setZoom(17);
            // }
            // this.marker.setPosition(place.geometry.location);
            // console.log('.......................' + place.geometry.location);
            // this.geocodeLatLng(this.geocoder, place.geometry.location.lat(), place.geometry.location.lng());
        });


        //Autocomplete  --End
    }

    locationSelect = (location) => {
        if (this.callBack) {
            this.callBack(parseFloat(location.location_lat), parseFloat(location.location_lng));
            this.cancelClick();
        }
    }

    cancelClick() {
        this.navCtrl.pop();
    }
}
