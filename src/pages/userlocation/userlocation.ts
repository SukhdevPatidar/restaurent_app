import {Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ActionSheetController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {ApiProvider} from './../../providers/api/api';
import {AutocompletePage} from '../autocomplete/autocomplete';
import {LocationAccuracy} from '@ionic-native/location-accuracy';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the UserlocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
    selector: 'page-userlocation',
    templateUrl: 'userlocation.html',
})
export class UserlocationPage {
    @ViewChild('map') mapElement: ElementRef;
    // @ViewChild('search') searchElement: ElementRef;
    map: any;
    marker: any;
    geocoder: any;
    count = 0;
    isFromRestaurenPage = false;

    showSearchBar = true;

    placeName = "";
    placeLat = "";
    placeLng = "";

    myLocations = [];
    callBack;

    constructor(
        private ga: GoogleAnalytics,
        public platform: Platform,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,
        public zone: NgZone,
        public actionSheetCtrl: ActionSheetController,
        public geolocation: Geolocation,
        private locationAccuracy: LocationAccuracy
    ) {
        this.isFromRestaurenPage = navParams.get("isFromRestaurenPage");
        this.callBack = navParams.get("callBack");
    }

    ionViewDidLoad() {
        this.ga.trackView("USERLOCATION");

        console.log('ionViewDidLoad UserlocationPage');
        this.getMyLocations();
        this.platform.ready().then(() => {
            this.loadMap();
        });
    }

    changeLocation = (latitude, longitude, formattedAddress = null) => {
        var latLng = new google.maps.LatLng(latitude, longitude);
        this.map.setCenter(latLng);
        this.map.setZoom(15);
        this.marker.setPosition(latLng);
        if (!formattedAddress) {
            this.geocodeLatLng(this.geocoder, latitude, longitude);
        } else {
            this.placeName = formattedAddress;
            this.placeLat = latitude;
            this.placeLng = longitude;
        }
    }

    openAutoComplete() {
        var callBack = (latitude, longitude, formattedAddress) => {
            this.changeLocation(latitude, longitude, formattedAddress);
        };
        this.navCtrl.push(AutocompletePage, {
            map: this.map,
            callBack: callBack,
            myLocations: this.myLocations,
            placeName: this.placeName
        });
    }

    sendPosition() {
        if (this.callBack) {
            this.callBack({
                placeName: this.placeName,
                placeLat: this.placeLat,
                placeLng: this.placeLng
            });
            this.navCtrl.pop();
        }
    }

    saveLocationAs() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'SALVA INDIRI',
            buttons: [

                {
                    text: 'CASA',
                    role: 'home',
                    handler: () => {
                        console.log('home clicked');
                        this.saveLocation("H");
                    }
                },
                {
                    text: 'ALTRO',
                    role: 'office',
                    handler: () => {
                        console.log('office clicked');
                        this.saveLocation("O");
                    }
                },
                {
                    text: 'CANCELLA',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        actionSheet.present();
    }


    saveLocation = (type) => {
        let user: any = localStorage.getItem("user_data");
        if (user) {
            user = JSON.parse(user);
        }

        var params = {
            user_id: user.user_id,
            location_type: type,
            location_address: this.placeName,
            location_lat: this.placeLat,
            location_lng: this.placeLng,
        };
        this.apiProvider.loading();

        this.apiProvider.postRequest(ApiProvider.METHODS.saveLocation, params).then((response: any) => {
            this.apiProvider.hideLoading();
            if (response.status) {
                this.myLocations = response.data;
                this.apiProvider.showToast("Posizione Salvata");
                if (this.isFromRestaurenPage) {
                    this.sendPosition();
                }
            }
        }, (error) => {
            this.apiProvider.hideLoading();
        });
    }


    getMyLocations = () => {
        let user: any = localStorage.getItem("user_data");
        if (user) {
            user = JSON.parse(user);
        } else {
            user = {
                user_id: 1
            };
        }

        var params = {
            user_id: user.user_id,
        };

        this.apiProvider.getRequest(ApiProvider.METHODS.getMyLocations, params).then((response: any) => {
            if (response.status) {
                this.myLocations = response.data;
            } else {
                this.myLocations = [];
            }
        }, (error) => {
        });
    }

    loadMap() {
        this.geocoder = new google.maps.Geocoder;
        var latLng = new google.maps.LatLng(22.7196, 75.8577);
        var geocoder = new google.maps.Geocoder;

        if (this.apiProvider.isDevice()) {
            var options = {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            };
            this.geolocation.getCurrentPosition(options).then((position) => {
                this.changeLocation(position.coords.latitude, position.coords.longitude);
                latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            }, (err) => {
                console.log(err);
            });
        }


        let mapOptions = {
            center: latLng,
            zoom: 15,
            panControl: true,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        google.maps.event.trigger(this.map, 'resize')

        var lat = latLng.lat();
        var lng = latLng.lng();
        if (this.count != 1) {
            this.addMarker(lat, lng);
            this.count = 1;
        }

        google.maps.event.addListener(this.map, "click", (event) => {
            var myLatLng = event.latLng;
            var latlng = new google.maps.LatLng(myLatLng.lat(), myLatLng.lng());
            this.marker.setPosition(latlng);
            this.geocodeLatLng(this.geocoder, myLatLng.lat(), myLatLng.lng());
        });

        //Autocomplete
        // var input = document.querySelector('#search input');
        // var searchBox = new google.maps.places.Autocomplete(input);
        // var autocomplete = new google.maps.places.Autocomplete(input);
        // autocomplete.bindTo('bounds', this.map);
        // autocomplete.addListener('place_changed', (event) => {
        //   var place = autocomplete.getPlace();
        //   if (!place.geometry) {
        //     console.log("No details available for input: '" + place.name + "'");
        //     return;
        //   }
        //
        //   if (place.geometry.viewport) {
        //     this.map.fitBounds(place.geometry.viewport);
        //   } else {
        //     this.map.setCenter(place.geometry.location);
        //     this.map.setZoom(17);
        //   }
        //   this.marker.setPosition(place.geometry.location);
        //   console.log('.......................' + place.geometry.location);
        //   this.geocodeLatLng(this.geocoder, place.geometry.location.lat(), place.geometry.location.lng());
        // });
        //Autocomplete  --End


    }

    currentPosition() {
        if (this.apiProvider.isDevice()) {
            var options = {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            };
            this.geolocation.getCurrentPosition(options).then((position) => {
                this.changeLocation(position.coords.latitude, position.coords.longitude);
            }, (err) => {
                console.log(err);
            });
        }
    }

    addMarker(lat, lng) {
        this.geocodeLatLng(this.geocoder, lat, lng);
        this.marker = new google.maps.Marker({
            map: this.map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng),
        });
        google.maps.event.addListener(this.marker, 'dragend', (marker) => {
            var latLng = marker.latLng;
            this.geocodeLatLng(this.geocoder, latLng.lat(), latLng.lng());
        });
    }

    geocodeLatLng(geocoder, lat, lng) {
        var latlng = {lat, lng};
        geocoder.geocode({'location': latlng}, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    this.zone.run(() => {
                        this.placeName = results[0].formatted_address;
                        this.placeLat = lat;
                        this.placeLng = lng;
                        console.log(this.placeName);
                    });
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }

    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });
    }

}
