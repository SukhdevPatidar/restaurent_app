import { Component , ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the LocationmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var google;

@IonicPage()
@Component({
  selector: 'page-locationmap',
  templateUrl: 'locationmap.html',
})
export class LocationmapPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker: any;
  placeLat = "";
  placeLng = "";

  constructor(
      private ga: GoogleAnalytics,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
  ) {
    this.placeLat = navParams.get("latitude");
    this.placeLng = navParams.get("longitude");
  }

  ionViewDidLoad() {
      this.ga.trackView("LOCATION_MAP");

      console.log('ionViewDidLoad LocationmapPage');

    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  loadMap() {
    var latLng = new google.maps.LatLng(this.placeLat, this.placeLng);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      panControl: false,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      overviewMapControl: false,
      rotateControl: false,
      fullscreenControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    google.maps.event.trigger(this.map, 'resize')

    var lat = latLng.lat();
    var lng = latLng.lng();
    this.addMarker(lat, lng);
  }


  addMarker(lat, lng) {
    this.marker = new google.maps.Marker({
      map: this.map,
      draggable: false,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(lat, lng),
    });
  }

}
