import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UtilProvider } from './../../providers/util/util';

/**
 * Generated class for the PhonecodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phonecode',
  templateUrl: 'phonecode.html',
})
export class PhonecodePage {

  countries = [];
searchInput = "";
countryCode = "";
isViewLoaded = false;


  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public utilProvider: UtilProvider,
  ) {
    this.countries = utilProvider.countries;
    this.countryCode = navParams.get('key');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhonecodePage');
    this.isViewLoaded = true;
  }

  close() {
    this.viewCtrl.dismiss(this.countryCode);
  }

  getCountryCode(code) {
    console.log("CountryCode", code);
    localStorage.setItem("CountryCode", code);
    this.countryCode = code;
    this.viewCtrl.dismiss(this.countryCode);
  }

}
