import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhonecodePage } from './phonecode';

@NgModule({
  declarations: [
    PhonecodePage,
  ],
  imports: [
    IonicPageModule.forChild(PhonecodePage),
  ],
})
export class PhonecodePageModule {}
