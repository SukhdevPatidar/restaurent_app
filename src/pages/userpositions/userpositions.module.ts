import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserpositionsPage } from './userpositions';

@NgModule({
  declarations: [
    UserpositionsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserpositionsPage),
  ],
})
export class UserpositionsPageModule {}
