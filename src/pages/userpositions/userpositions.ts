import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {LocationmapPage} from '../locationmap/locationmap';
import {DriversMapPage} from '../drivers-map/drivers-map';
import {LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator';
import {Geolocation} from '@ionic-native/geolocation';
import {GoogleAnalytics} from '@ionic-native/google-analytics';
import {ApiProvider} from "../../providers/api/api";

/**
 * Generated class for the UserpositionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-userpositions',
    templateUrl: 'userpositions.html',
})
export class UserpositionsPage {
    posotionsList = [];
    restaurentData;

    currentLat = 37.32524099;
    currentLng = -122.02507159;

    watchLocation;

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public platform: Platform,
        public navParams: NavParams,
        private launchNavigator: LaunchNavigator,
        private geolocation: Geolocation,
        private events: Events,
        public apiProvider: ApiProvider
    ) {
        this.posotionsList = this.navParams.get("posotionsList");
        this.restaurentData = this.navParams.get("restaurentData");
    }

    ionViewDidLoad() {
        this.ga.trackView("USER_POSITIONS");

        console.log('ionViewDidLoad UserpositionsPage');
        this.platform.ready().then(() => {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.currentLat = resp.coords.latitude;
                this.currentLng = resp.coords.longitude;
                console.log('Location', resp);
            }).catch((error) => {
                console.log('Error getting location', error);
            });

            this.watchLocation = this.geolocation.watchPosition()
                .filter((p) => p.coords !== undefined)
                .subscribe((data) => {
                    console.log('Location', data);
                    this.currentLat = data.coords.latitude;
                    this.currentLng = data.coords.longitude;
                });
        });

        this.events.subscribe("UpdateRestaurentLocations", () => {
            this.searchRes(this.restaurentData.restaurent_id);
        });
    }


    ionViewWillUnload() {
        // To stop notifications
        this.watchLocation.unsubscribe();
        this.events.unsubscribe("UpdateRestaurentLocations");
    }


    searchRes = (resId, busy = true, callBack = null) => {
        localStorage.setItem("setting_res_id", resId);

        var params = {
            restaurent_id: resId
        };

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getResUserPositions, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                var list = response.data;
                this.navCtrl.push(UserpositionsPage, {posotionsList: list, restaurentData: response.restaurent});
            } else {
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }

    openMap(position) {
        this.navCtrl.push(LocationmapPage, {
            latitude: position.restaurants_user_location_lat,
            longitude: position.restaurants_user_location_lng
        });
    }

    goNavigation(position) {

        // let url ="https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=" + this.origin.location.lat + "," + this.origin.location.lng+"&destination=" + this.desLat + "," + this.desLng;

        // if(this.platform.is('android')){
        //   let options = "hardwareback=no";
        //   this.browser = this.iab.create(url, "_blank", options);
        // }else {
        //   this.browser = this.iab.create(url, "_blank");
        // }
        // this.browser.on('exit').subscribe(event => {
        //   console.log("event", event);
        //   this.browser.close();
        // console.log("Appbrowser closed");
        // },error => {
        //   console.log("error", error);
        //   this.browser.close();
        // });

        var start = [];
        var end = [];
        if (this.currentLat && this.currentLng) {
            start.push(this.currentLat);
            start.push(this.currentLng);
        } else {
            start.push(position.restaurants_user_location_lat);
            start.push(position.restaurants_user_location_lng);
        }

        end.push(position.restaurants_user_location_lat);
        end.push(position.restaurants_user_location_lng);

        let options: LaunchNavigatorOptions = {
            start: start,
            app: this.launchNavigator.APP.GOOGLE_MAPS
        };
        this.launchNavigator.navigate(end, options)
            .then(success => {
                console.log(success);
            }, error => {
                console.log(error);
            })
    }

    goDriversMap(position) {
        this.navCtrl.push(DriversMapPage, {
            my_location: {
                latitude: this.currentLat,
                longitude: this.currentLng
            },
            position: position,
            restaurent: this.restaurentData,
            callBack: (driver) => {
                position['driver_id'] = driver.user_id;
                position['request_status'] = "P";
            }
        });
    }

    viewDriverTracking(position) {
        this.navCtrl.push(DriversMapPage, {
            my_location: {
                latitude: this.currentLat,
                longitude: this.currentLng
            },
            position: position,
            restaurent: this.restaurentData,
            driver_tracking: true
        });
    }

    getDateStr(dateStr) {
        return dateStr.replace(" ", "T");
    }

    getRequestStatus(status) {
        if (status == "P") {
            return "Pending";
        } else if (status == "A") {
            return "Accepted";
        } else if (status == "R") {
            return "Rejected";
        } else if (status == "F") {
            return "Completed";
        }
        return "Pending";
    }

}
