import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriversMapPage } from './drivers-map';

@NgModule({
  declarations: [
    DriversMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DriversMapPage),
  ],
})
export class DriversMapPageModule {}
