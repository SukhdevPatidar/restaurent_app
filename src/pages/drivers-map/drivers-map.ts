import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import {Geolocation} from "@ionic-native/geolocation";
import set = Reflect.set;
import {FirebaseProvider} from "../../providers/firebase/firebase";

/**
 * Generated class for the DriversMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
    selector: 'page-drivers-map',
    templateUrl: 'drivers-map.html',
})
export class DriversMapPage {
    @ViewChild('map') mapElement: ElementRef;

    myLocation;
    position;
    restaurent;
    drives = [];
    driver_tracking = false;

    map: any;
    geocoder: any;

    currentPosMarker;
    userPosMarker;
    drivesMarker = [];
    userPathPolyline;

    isBoundsSetFirstTime = false;
    driverUpdateTimer;
    callBack;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public platform: Platform,
        public apiProvider: ApiProvider,
        private ga: GoogleAnalytics,
        public geolocation: Geolocation,
        private firebaseProvider: FirebaseProvider,
    ) {
        this.myLocation = navParams.get("my_location");
        this.position = navParams.get("position");
        this.restaurent = navParams.get("restaurent");
        this.driver_tracking = navParams.get("driver_tracking");
        this.callBack = navParams.get("callBack");
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DriversMapPage');

        this.platform.ready().then(() => {
            this.loadMap();
            this.getDrivers();

            this.driverUpdateTimer = setInterval(() => {
                this.getDrivers();
            }, 1000 * 15);
        });
    }

    ionViewWillUnload() {
        // To stop notifications
        clearInterval(this.driverUpdateTimer);
    }

    getDrivers = () => {
        var params = {
            restaurent_id: this.position.restaurants_user_location_res_id,
        };

        if (this.driver_tracking) {
            params['driver_id'] = this.position.driver_id;
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getDrivers, params).then((response: any) => {
            if (response.status) {
                this.drives = response.data;
            } else {
                this.drives = [];
            }
            this.setDriversOnMap();
        }, (error) => {
        });
    }

    getRoutes = () => {
        let originCoords = {
            lat: this.myLocation.latitude,
            lng: this.myLocation.longitude,
        };

        let destCoords = {
            lat: this.position.restaurants_user_location_lat,
            lng: this.position.restaurants_user_location_lng,
        };


        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({
            map: this.map,
            preserveViewport: true
        });

        directionsService.route({
            origin: new google.maps.LatLng(originCoords.lat, originCoords.lng),
            destination: new google.maps.LatLng(destCoords.lat, destCoords.lng),
            waypoints: [{
                stopover: false,
                location: new google.maps.LatLng(originCoords.lat, originCoords.lng)
            }],
            travelMode: google.maps.TravelMode.DRIVING
        }, (response, status) => {

            if (status === google.maps.DirectionsStatus.OK) {
                this.userPathPolyline = new google.maps.Polyline({
                    path: [],
                    strokeColor: '#A82128',
                    strokeWeight: 3
                });
                var bounds = new google.maps.LatLngBounds();

                var legs = response.routes[0].legs;
                for (var i = 0; i < legs.length; i++) {
                    var steps = legs[i].steps;
                    for (var j = 0; j < steps.length; j++) {
                        var nextSegment = steps[j].path;
                        for (var k = 0; k < nextSegment.length; k++) {
                            this.userPathPolyline.getPath().push(nextSegment[k]);
                            bounds.extend(nextSegment[k]);
                        }
                    }
                }
                this.map.fitBounds(bounds);
                this.userPathPolyline.setMap(this.map);
            } else {
                // window.alert('Directions request failed due to ' + status);
            }
        });

    }

    setDriversOnMap = () => {
        var bounds = new google.maps.LatLngBounds();

        var latlng = new google.maps.LatLng(this.myLocation.latitude, this.myLocation.longitude);
        bounds.extend(latlng);
        if (this.driver_tracking) {
            var latlng = new google.maps.LatLng(this.position.restaurants_user_location_lat, this.position.restaurants_user_location_lng);
            bounds.extend(latlng);
        }

        this.drives.map((driver) => {
            var latlng = new google.maps.LatLng(driver.latitude, driver.longitude);
            bounds.extend(latlng);

            var found = false;
            for (var i = 0; i < this.drivesMarker.length; i++) {
                if (this.drivesMarker[i].customInfo.driverId == driver.user_id) {
                    found = true;
                    this.drivesMarker[i].setPosition(latlng);
                    break;
                }
            }
            if (!found) {
                this.addDriverMarker(driver.latitude, driver.longitude, driver);
            }
        });

        let tmpDriversMarker = this.drivesMarker;
        tmpDriversMarker.map((marker, key) => {
            var found = false;
            for (var i = 0; i < this.drives.length; i++) {
                if (marker.customInfo.driverId == this.drives[i].user_id) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                marker.setMap(null);
                this.drivesMarker.splice(key, 1);
            }
        });


        if (!this.isBoundsSetFirstTime) {
            this.isBoundsSetFirstTime = true;
            this.map.fitBounds(bounds);
        }
    }

    loadMap() {
        var latLng = new google.maps.LatLng(this.myLocation.latitude, this.myLocation.longitude);
        var geocoder = new google.maps.Geocoder;

        if (this.apiProvider.isDevice()) {
            var options = {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            };
            this.geolocation.getCurrentPosition(options).then((position) => {
                this.changeLocation(position.coords.latitude, position.coords.longitude);
                latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            }, (err) => {
                console.log(err);
            });
        }


        let mapOptions = {
            center: latLng,
            zoom: 15,
            panControl: true,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        google.maps.event.trigger(this.map, 'resize');

        var lat = latLng.lat();
        var lng = latLng.lng();
        this.addCurrentPosMarker(lat, lng);
        this.createCircle({
            lat: lat,
            lng: lng
        }, this.map, "#4285F4");

        if (this.driver_tracking) {
            this.setUserLocationMarker();
            this.getRoutes();
        }
    }

    changeLocation = (latitude, longitude, formattedAddress = null) => {
        var latLng = new google.maps.LatLng(latitude, longitude);
        this.map.setCenter(latLng);
        this.map.setZoom(15);
        this.currentPosMarker.setPosition(latLng);
    }


    addCurrentPosMarker = (lat, lng) => {
        let marker = new google.maps.Marker({
            map: this.map,
            draggable: false,
            // icon: {
            //     path: google.maps.SymbolPath.CIRCLE,
            //     fillColor: "#4285F4",
            //     fillOpacity: 1.0,
            //     strokeColor: "#fff",
            //     strokeOpacity: 1.0,
            //     strokeWeight: 0,
            //     scale: 10
            // },
            icon: "./assets/images/restaurant_marker.png",
            position: new google.maps.LatLng(lat, lng),
        });

        google.maps.event.addListener(marker, 'dragend', (marker) => {
            var latLng = marker.latLng;
        });
        this.currentPosMarker = marker;
    }

    setUserLocationMarker() {
        if (!this.userPosMarker) {
            let marker = new google.maps.Marker({
                map: this.map,
                draggable: false,
                icon: "./assets/images/map_user.png",
                position: new google.maps.LatLng(this.position.restaurants_user_location_lat, this.position.restaurants_user_location_lng),
                customInfo: {
                    userId: this.position.user_id
                },
            });

            var content = "<b>User Name: </b> " + this.position.user_first;
            content = content + "<br/>";
            content = content + "<b>Contact No.: </b> " + this.position.user_phone;
            this.addInfoWindow(marker, content, this.position);
            this.userPosMarker = marker;
        }
    }

    addDriverMarker(lat, lng, driver) {
        let marker = new google.maps.Marker({
            map: this.map,
            draggable: false,
            icon: "./assets/images/driver_loc.png",
            position: new google.maps.LatLng(lat, lng),
            customInfo: {
                driverId: driver.user_id
            },
        });

        var content = "<b>Driver Name: </b> " + driver.user_first;
        content = content + "<br/>";
        content = content + "<b>Contact No.: </b> " + driver.user_phone;
        content = content + "<button style='padding: 6px 10px;margin-top: 5px;background-color: #7cad31;color: white;border-radius: 5px;' id='infowindo_" + driver.user_id + "' >Send Request</button>";

        this.addInfoWindow(marker, content, driver, () => {
            setTimeout(() => {
                let elem = document.getElementById("infowindo_" + driver.user_id);
                elem.addEventListener("click", (e) => {
                    e.stopImmediatePropagation();
                    this.sendRequestToDriver(driver);
                })
            }, 500);
        });
        this.drivesMarker.push(marker);
    }

    createCircle(location, map, color) {
        var _radius = 500;
        var rMin = _radius * 4 / 5;
        var rMax = _radius;
        var direction = 1;
        var circleOption = {
            center: location,
            fillColor: color,
            fillOpacity: 0.6,
            map: map,
            radius: 500,
            strokeColor: color,
            strokeOpacity: 1,
            strokeWeight: 0.5
        };
        var circle = new google.maps.Circle(circleOption);

        var circleTimer = setInterval(function () {
            var radius = circle.getRadius();

            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            var _par = (radius / _radius) - 0.7;

            circleOption.radius = radius + direction * 10;
            circleOption.fillOpacity = 0.6 * _par;

            circle.setOptions(circleOption);
        }, 20);


    }

    addInfoWindow(marker, content, driver, callBack = null) {
        let infoWindow = new google.maps.InfoWindow({
            content: content
        });

        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
            if (callBack) {
                callBack();
            }
        });
    }

    sendRequestToDriver(driver) {
        let sender = this.apiProvider.getUser();

        var params = {
            sender_id: sender.user_id,
            driver_id: driver.user_id,
            restaurent_id: this.restaurent.restaurent_id,
            restaurants_user_location_id: this.position.restaurants_user_location_id,
        };

        this.apiProvider.loading();
        this.apiProvider.postRequest(ApiProvider.METHODS.sendDriverRequest, params).then((response: any) => {
            this.apiProvider.hideLoading();
            if (response.status) {
                this.apiProvider.showToast(response.message);
                if (this.callBack) {
                    this.callBack(driver);
                }
                if (driver.push_token) {
                    let message = "You have a delivery request recieved from " + this.restaurent.restaurent_name;
                    this.firebaseProvider.sendDeliveryRequestNotification(driver.push_token, sender.user_first, sender.user_id, message, response.data);
                }

                setTimeout(() => {
                    this.navCtrl.pop();
                }, 1000);
            } else {
                this.apiProvider.showToast(response.message);
            }
        }, (error) => {
            this.apiProvider.hideLoading();
        });
    }

}
