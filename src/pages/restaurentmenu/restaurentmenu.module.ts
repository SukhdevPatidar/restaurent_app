import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurentmenuPage } from './restaurentmenu';

@NgModule({
  declarations: [
    RestaurentmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurentmenuPage),
  ],
})
export class RestaurentmenuPageModule {}
