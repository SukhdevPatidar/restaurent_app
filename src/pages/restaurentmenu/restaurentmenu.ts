import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from './../../providers/api/api';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the RestaurentmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-restaurentmenu',
    templateUrl: 'restaurentmenu.html',
})
export class RestaurentmenuPage {
    restaurent;
    resMenuList = [];

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider
    ) {
        this.restaurent = navParams.get("restaurent");
    }

    ionViewDidLoad() {
        this.ga.trackView("RESTAURENT_MENUS");
        console.log('ionViewDidLoad RestaurentmenuPage');
        this.getMenus(this.restaurent.restaurent_id);
    }

    getMenus = (resId, busy = true, callBack = null) => {
        var params = {
            restaurent_id: resId
        };

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.resMenuList, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                var resMenuList = [];
                var list = response.data;
                list.map((resMenu) => {
                    if (resMenu.items && resMenu.items.length > 0) {
                        resMenuList.push(resMenu);
                    }
                });
                this.resMenuList = resMenuList;
                if (callBack) {
                    callBack();
                }
            } else {
                this.resMenuList = [];
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }


    formatPrice(price) {
        return parseFloat(price).toFixed(2);
    }
}
