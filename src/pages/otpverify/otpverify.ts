import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SidemenuPage } from '../sidemenu/sidemenu';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
import { ApiProvider } from './../../providers/api/api';



/**
 * Generated class for the OtpverifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otpverify',
  templateUrl: 'otpverify.html',
})
export class OtpverifyPage {
  verificationid;
  code;
  phoneNumber = "";
  callBack: any;

  progress = 0;
  otpValidating = false;
  otpValidatingError = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
  ) {
    this.verificationid = navParams.get('verificationid');
    this.phoneNumber = navParams.get('phone');
    this.callBack = navParams.get('callBack');
  }

  ionViewDidLoad() {
    var length = 2*60*1000;
    console.log('ionViewDidLoad OtpverifyPage');
    var timer = setInterval(() => {
      this.progress += 1;
      if (this.progress >= 100) {
        clearInterval(timer);
        this.navCtrl.pop();
      }
    }, length/100);
  }

  onCodeChage() {
    var codeStr = this.code;
    console.log(codeStr);
    if(codeStr) {
      codeStr = codeStr.toString();
    }
    if (codeStr && codeStr.length >= 6) {
      if(!this.otpValidating) {
        this.otpValidatingError = false;
      this.apiProvider.loading();
      this.otpValidating = true;
      let signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationid, codeStr);
      firebase.auth().signInAndRetrieveDataWithCredential(signInCredential).then((info) => {
        this.otpValidating = false;
        this.apiProvider.hideLoading();
            if (info.user) {
              this.otpValidatingError = false;
              console.log(info);
              this.navCtrl.pop();
              this.callBack();
            } else {
              this.otpValidatingError = true;
            }
          }, (error) => {
            this.otpValidatingError = true;
            this.apiProvider.hideLoading();
            this.otpValidating = false;
              console.log(error);
          });
      }
    }
  }
}
