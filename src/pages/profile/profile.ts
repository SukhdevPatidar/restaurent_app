import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ActionSheetController} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {ApiProvider} from './../../providers/api/api';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    perview = "";
    user_photo = "";

    user: any = {};
    citiesList = [];

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public navParams: NavParams,
        private camera: Camera,
        public actionSheetCtrl: ActionSheetController,
        public apiProvider: ApiProvider,
    ) {
        let user: any = localStorage.getItem("user_data");
        if (user) {
            this.user = JSON.parse(user);
        }
    }

    ionViewDidLoad() {
        this.ga.trackView("PROFILE");
        console.log('ionViewDidLoad ProfilePage');

        this.getCities(false);

    }


    getCities = (busy = true, callBack = null) => {
        var params = {};
        // if (this.speciality) {
        //   params['speciality'] = this.speciality;
        // }

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getCities, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.citiesList = response.data;
                if (callBack) {
                    callBack();
                }
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }


    changeImage() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Seleziona',
            buttons: [
                {
                    text: 'Camera',

                    handler: () => {
                        this.pickPhoto(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Galleria',
                    handler: () => {
                        this.pickPhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Cancella',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        actionSheet.present();
    }


    pickPhoto(sourceType) {
        const options: CameraOptions = {
            quality: 25,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: sourceType,
        }

        this.camera.getPicture(options).then((imageData) => {
            this.user_photo = imageData;
            this.perview = "data:image/jpeg;base64," + imageData;
        }, (err) => {
            // Handle error
        });
    }


    saveUser = () => {

        var userData = {
            'user_id': this.user.user_id,
            'user_first': this.user.user_first,
            'user_phone': this.user.user_phone,
            'user_city_id': this.user.user_city_id,
        };

        if (this.user_photo) {
            userData['user_photo'] = this.user_photo;
        }

        this.apiProvider.loading();

        this.apiProvider.postRequest(ApiProvider.METHODS.updateProfile, userData).then((response: any) => {
            this.apiProvider.hideLoading();
            if (response.status) {
                this.user = response.data;
                localStorage.setItem("user_data", JSON.stringify(response.data));
                this.apiProvider.showToast("Profilo Aggiornato");
            }
        }, (error) => {
            this.apiProvider.hideLoading();
        });
    }

}
