import {Component, ViewChild} from '@angular/core';
import {Nav, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {CallNumber} from '@ionic-native/call-number';

import {CategoriesPage} from '../categories/categories';
import {HomePage} from '../home/home';
import {RestaurentmenuPage} from '../restaurentmenu/restaurentmenu';
import {ProfilePage} from '../profile/profile';
import {OtpverifyPage} from '../otpverify/otpverify';
import {RestaurentsPage} from '../restaurents/restaurents';
import {UserlocationPage} from '../userlocation/userlocation';
import {SettingPage} from '../setting/setting';
import {ApiProvider} from './../../providers/api/api';


/**
 * Generated class for the SidemenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-sidemenu',
    templateUrl: 'sidemenu.html',
})
export class SidemenuPage {
    @ViewChild(Nav) nav: Nav;
    rootPage = CategoriesPage;

    CategoriesPage = CategoriesPage;
    HomePage = HomePage;
    RestaurentmenuPage = RestaurentmenuPage;
    ProfilePage = ProfilePage;
    OtpverifyPage = OtpverifyPage;
    RestaurentsPage = RestaurentsPage;
    UserlocationPage = UserlocationPage;
    SettingPage = SettingPage;

    user = {};

    canLoginSkip = true;


    constructor(
        public platform: Platform,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,
        private callNumber: CallNumber
    ) {

        let canLoginSkip = localStorage.getItem("canLoginSkip");

        if (canLoginSkip != undefined) {
            if (parseInt(canLoginSkip) == 1) {
                this.canLoginSkip = true;
            } else {
                this.canLoginSkip = false;
            }
        }

        platform.ready().then(() => {
            platform.registerBackButtonAction((e) => {
                if (!this.apiProvider.isShowLoading) {
                    if (this.nav.canGoBack()) {
                        this.nav.pop();
                    }
                }
                return !this.apiProvider.isShowLoading;
            });
        });
    }


    getUser() {
        let user: any = localStorage.getItem("user_data");
        if (user) {
            user = JSON.parse(user);
        } else {
            user = {user_first: "Welcome Guest"};
        }
        return user;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SidemenuPage');
    }

    ionViewWillUnload() {
    }

    logout() {
        localStorage.removeItem("user_data");
        this.nav.setRoot(HomePage);
    }

    openPage(page) {
        this.nav.setRoot(page);
    }

    callVelofood() {
        const number = "+393389810104";

        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

}
