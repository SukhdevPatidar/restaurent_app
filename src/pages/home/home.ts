import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, ActionSheetController, Events} from 'ionic-angular';
import {OtpverifyPage} from '../otpverify/otpverify';
import {CategoriesPage} from '../categories/categories';
import {SidemenuPage} from '../sidemenu/sidemenu';
import {Camera, CameraOptions} from '@ionic-native/camera';

import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase1 from 'firebase/app';
import AuthProvider = firebase1.auth.AuthProvider;
import {Firebase} from '@ionic-native/firebase';
import {ApiProvider} from './../../providers/api/api';
import {UtilProvider} from './../../providers/util/util';

import {PhonecodePage} from '../phonecode/phonecode';
import {GoogleAnalytics} from '@ionic-native/google-analytics';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    OtpverifyPage = OtpverifyPage;
    CategoriesPage = CategoriesPage;
    SidemenuPage = SidemenuPage;
    currentCountryCode = "IT";
    currentCountryName = "Italy";
    currentDialCode = "+39"
    imageUrl = "./assets/flags/it.svg"

    citiesList = [];


    phone = "";
    fname = "";
    isVerfied = false;
    city = 5;

    //Test
    isTest = false;

    loggingIn = false;

    private user: firebase1.User;
    termsCondition = false;


    perview = "";
    user_photo = "";


    canLoginSkip = true;

    constructor(
        private ga: GoogleAnalytics,
        public afAuth: AngularFireAuth,
        public navCtrl: NavController,
        private firebase: Firebase,
        public apiProvider: ApiProvider,
        public utilProvider: UtilProvider,
        public modalCtrl: ModalController,
        private camera: Camera,
        public actionSheetCtrl: ActionSheetController,
        private events: Events,
    ) {
        let canLoginSkip = localStorage.getItem("canLoginSkip");

        if (canLoginSkip != undefined) {
            if (parseInt(canLoginSkip) == 1) {
                this.canLoginSkip = true;
            } else {
                this.canLoginSkip = false;
            }
        }

        if (this.isTest) {
            this.phone = "9981131029";
            this.fname = "Sukhdev";
            this.isVerfied = true;
        }

    }


    ionViewDidEnter() {
        let user = localStorage.getItem("user_data");
        if (user) {
            this.goToDashboard();
        }
        this.getCities(false);
        // this.recaptchaVerifier = new firebase1.auth.RecaptchaVerifier('sign-in-button', {
        //   'size': 'invisible',
        //   'callback': (response) => {
        //     debugger;
        //     console.log("onSignInSubmit");
        //     this.signIn1();
        //   }
        // });
    }

    isValidate = () => {
        if (this.isVerfied) {
            if (this.fname && this.fname.length > 0 && this.city) {
                return true;
            }
        } else if (this.termsCondition && this.phone && this.currentDialCode && this.currentDialCode.length > 0) {
            return true;
        }
        return false;
    }

    onVerified = () => {
        this.isVerfied = true;
        this.checkMobileNo();
    }


    goToDashboard = (data = null) => {
        this.apiProvider.setUser(data);
        this.navCtrl.push(SidemenuPage);
        this.ga.setUserId(this.apiProvider.getUser().user_id);
        this.events.publish("LOGGED_IN");
    }

    checkMobileNo = (busy = true) => {
        this.currentDialCode = this.currentDialCode.replace("+", "");
        let tell = "+" + this.currentDialCode + "" + this.phone;

        var params = {
            mobile: tell
        };

        if (busy) {
            this.apiProvider.loading();
        }
        this.apiProvider.postRequest(ApiProvider.METHODS.checkMobileNo, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.goToDashboard(response.data);
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }

    getCities = (busy = true, callBack = null) => {
        var params = {};
        // if (this.speciality) {
        //   params['speciality'] = this.speciality;
        // }

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getCities, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.citiesList = response.data;
                if (callBack) {
                    callBack();
                }
            }
            this.canLoginSkip = response.canLoginSkip;
            localStorage.setItem("canLoginSkip", response.canLoginSkip);
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }


    signIn() {
        if (this.isVerfied) {
            if (this.fname && this.fname.length > 0 && this.city) {
                this.currentDialCode = this.currentDialCode.replace("+", "");
                let tell = "+" + this.currentDialCode + "" + this.phone;

                var params = {
                    fname: this.fname,
                    lname: " ",
                    city: this.city,
                    mobile: tell,
                };

                if (this.user_photo) {
                    params['user_photo'] = this.user_photo;
                }

                this.apiProvider.loading();

                this.apiProvider.postRequest(ApiProvider.METHODS.appSignin, params).then((response: any) => {
                    this.apiProvider.hideLoading();
                    if (response.status) {
                        this.goToDashboard(response.data);
                    }
                }, (error) => {
                    this.apiProvider.hideLoading();
                });
            }
        } else {
            if (this.termsCondition && this.phone && this.currentDialCode && this.currentDialCode.length > 0) {
                this.currentDialCode = this.currentDialCode.replace("+", "");
                let tell = "+" + this.currentDialCode + "" + this.phone;
                this.loggingIn = true;
                this.firebase.verifyPhoneNumber(tell, 60).then((credential) => {
                    this.loggingIn = false;
                    console.log(credential);
                    var verificationId;
                    if (typeof credential == "string") {
                        verificationId = credential;
                    } else {
                        verificationId = credential.verificationId;
                    }
                    this.navCtrl.push(OtpverifyPage, {
                        verificationid: verificationId,
                        phone: tell,
                        callBack: this.onVerified
                    });
                }, (error) => {
                    this.loggingIn = false;
                    console.error(error);
                });
                // this.afAuth.auth.signInWithPhoneNumber(tell, this.recaptchaVerifier).then((credential) => {
                // debugger;
                //   let verificationId = credential.verificationId;
                //   this.navCtrl.push(OtpverifyPage, { verificationid: verificationId, phone: tell });
                // }, (error) => {
                //   debugger;
                //   console.error(error);
                // });
            }
        }
    }


    openCountryModal = () => {
        if (!this.isVerfied) {
            let key = this.currentCountryCode
            let countryModal = this.modalCtrl.create(PhonecodePage);
            countryModal.present();
            countryModal.onDidDismiss(data => {
                console.log("modalData", data);
                this.getCountryCode(data);
            });
        }
    }


    changeImage() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Seleziona',
            buttons: [
                {
                    text: 'Camera',

                    handler: () => {
                        this.pickPhoto(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Galleria',
                    handler: () => {
                        this.pickPhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Cancella',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        actionSheet.present();
    }


    pickPhoto(sourceType) {
        const options: CameraOptions = {
            quality: 25,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: sourceType,
        }

        this.camera.getPicture(options).then((imageData) => {
            this.user_photo = imageData;
            this.perview = "data:image/jpeg;base64," + imageData;
        }, (err) => {
            // Handle error
        });
    }


    getCountryCode(codeName) {
        console.log("countryName", codeName);
        this.utilProvider.getCountryCode(codeName).subscribe(result => {
            console.log("result", result);
            this.currentCountryCode = result.code;
            let code = this.currentCountryCode.toLowerCase();
            console.log("code", code);
            this.currentCountryName = result.name;
            this.currentDialCode = result.dial_code;
            this.imageUrl = "./assets/flags/" + code + ".svg";
        });
    }
}
