import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from './../../providers/api/api';
import {UserpositionsPage} from '../userpositions/userpositions';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-setting',
    templateUrl: 'setting.html',
})
export class SettingPage {

    resId;
    errorMessage = "";

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider
    ) {
    }

    ionViewDidLoad() {
        this.ga.trackView("SETTINGS");
        let resId = localStorage.getItem("setting_res_id");
        if (resId) {
            this.resId = resId;
            // this.searchRes(this.resId);
        }
        console.log('ionViewDidLoad SettingPage');
    }

    isValidate = () => {
        return (this.resId && this.resId.length >= 4);
    }

    checkResId() {
        if (this.isValidate()) {
            this.searchRes(this.resId);
        }
    }

    searchRes = (resId, busy = true, callBack = null) => {
        localStorage.setItem("setting_res_id", resId);

        var params = {
            restaurent_id: resId
        };

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getResUserPositions, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.errorMessage = "";
                var list = response.data;
                this.navCtrl.push(UserpositionsPage, {posotionsList: list, restaurentData: response.restaurent});
            } else {
                this.errorMessage = response.message;
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }
}
