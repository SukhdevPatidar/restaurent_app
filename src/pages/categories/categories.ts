import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {RestaurentsPage} from '../restaurents/restaurents';
import {ApiProvider} from './../../providers/api/api';
import {LocationAccuracy} from '@ionic-native/location-accuracy';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {
    RestaurentsPage = RestaurentsPage;
    resCatList = [];
    citiesList = [];
    selectedCityId = "";
    user: any = {};

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,
        private locationAccuracy: LocationAccuracy
    ) {
        this.apiProvider.navBar = this.navCtrl;
    }

    ionViewDidLoad() {
        this.ga.trackView("CATEGORIES");

        console.log('ionViewDidLoad CategoriesPage');
        let cityId = localStorage.getItem("selected_city");
        let user: any = localStorage.getItem("user_data");
        if (user) {
            this.user = JSON.parse(user);
        }

        if (cityId != undefined) {
            this.selectedCityId = cityId;
        } else {
            this.selectedCityId = this.user.user_city_id;
        }

        this.getCities(false);
        this.getResCategories();


        this.locationAccuracy.canRequest().then((canRequest: boolean) => {

            if (canRequest) {
                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                    () => console.log('Request successful'),
                    error => console.log('Error requesting location permissions', error)
                );
            }
        });
    }

    cityChanged() {
        console.log("cityChanged");
        localStorage.setItem("selected_city", this.selectedCityId);
        this.getResCategories(false);
    }

    getResCategories = (busy = true, callBack = null) => {
        var params = {};
        if (this.selectedCityId) {
            params['city_id'] = this.selectedCityId;
        }

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.resCatList, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                var resCatList = [];
                var list = response.data;
                list.map((cat) => {
                    if (cat.restaurant_count > 0) {
                        resCatList.push(cat);
                    }
                });
                this.resCatList = resCatList;

            } else {
                this.resCatList = [];
            }
            if (callBack) {
                callBack();
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }

    getCities = (busy = true, callBack = null) => {
        var params = {};
        // if (this.speciality) {
        //   params['speciality'] = this.speciality;
        // }

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.getCities, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.citiesList = response.data;
                if (callBack) {
                    callBack();
                }
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }

    openResCategory = (cat) => {
        this.navCtrl.push(RestaurentsPage, {category: cat, city_id: this.selectedCityId});
    }

}
