import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ActionSheetController} from 'ionic-angular';
import {RestaurentmenuPage} from '../restaurentmenu/restaurentmenu';
import {UserlocationPage} from '../userlocation/userlocation';
import {ApiProvider} from './../../providers/api/api';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

/**
 * Generated class for the RestaurentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-restaurents',
    templateUrl: 'restaurents.html',
})
export class RestaurentsPage {
    RestaurentmenuPage = RestaurentmenuPage;
    category;
    restaurentsList = [];
    cityId;

    constructor(
        private ga: GoogleAnalytics,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,
        public actionSheetCtrl: ActionSheetController
    ) {
        this.category = navParams.get("category");
        this.cityId = navParams.get("city_id");
    }

    ionViewDidLoad() {
        this.ga.trackView("RESTAURENTS");

        console.log('ionViewDidLoad RestaurentsPage');
        this.getRestaurents(this.category.category_id);
    }

    openMenu(restaurent) {
        this.navCtrl.push(RestaurentmenuPage, {restaurent: restaurent});
    }

    getRestaurents = (catId, busy = true, callBack = null) => {
        var params = {
            category_id: catId
        };
        if (this.cityId) {
            params['city_id'] = this.cityId;
        }

        if (busy) {
            this.apiProvider.loading();
        }

        this.apiProvider.getRequest(ApiProvider.METHODS.resList, params).then((response: any) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
            if (response.status) {
                this.restaurentsList = response.data;
                if (callBack) {
                    callBack();
                }
            }
        }, (error) => {
            if (busy) {
                this.apiProvider.hideLoading();
            }
        });
    }


    sendLocation = (restaurent) => {
        var callBack = (location) => {
            this.sendLocationToRestaurent(restaurent, location.placeName, location.placeLat, location.placeLng);
        };
        this.navCtrl.push(UserlocationPage, {isFromRestaurenPage: true, callBack: callBack});
    }

    // sendLocation11 = (restaurent) => {
    //   let actionSheet = this.actionSheetCtrl.create({
    //     title: 'Send Location!',
    //     buttons: [
    //
    //       {
    //         text: 'Home',
    //         role: 'home',
    //         handler: () => {
    //           console.log('home clicked');
    //           this.sendLocationToRestaurent(restaurent, "H");
    //         }
    //       },
    //       {
    //         text: 'Office',
    //         role: 'office',
    //         handler: () => {
    //           console.log('office clicked');
    //           this.sendLocationToRestaurent(restaurent, "O");
    //         }
    //       },
    //       {
    //         text: 'Cancel',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Cancel clicked');
    //         }
    //       }
    //     ]
    //   });
    //
    //   actionSheet.present();
    // }

    sendLocationToRestaurent = (restaurent, locationAddress, locationLat, locationLng) => {
        let user: any = localStorage.getItem("user_data");
        if (user) {
            user = JSON.parse(user);
        } else {
            user = {
                user_id: 1
            };
        }

        var params = {
            user_id: user.user_id,
            location_address: locationAddress,
            location_lat: locationLat,
            location_lng: locationLng,
            restaurent_id: restaurent.restaurent_id,
        };
        this.apiProvider.loading();

        this.apiProvider.postRequest(ApiProvider.METHODS.sendLocation, params).then((response: any) => {
            this.apiProvider.hideLoading();
            if (response.status) {
                this.apiProvider.showToast("Posizione inviata con successo");
            }
        }, (error) => {
            this.apiProvider.hideLoading();
        });
    }
}
