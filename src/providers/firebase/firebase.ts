import {Injectable} from '@angular/core';

import * as firebase from "firebase";
import {ApiProvider} from "../api/api";

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
@Injectable()
export class FirebaseProvider {

    static config = {
        apiKey: "AIzaSyBAJJZwdp1nN_tB8HCmrHGrXKmmF_t23sg",
        authDomain: "restaurents-bf7fd.firebaseapp.com",
        databaseURL: "https://restaurents-bf7fd.firebaseio.com",
        projectId: "restaurents-bf7fd",
        storageBucket: "restaurents-bf7fd.appspot.com",
        messagingSenderId: "70925429276",
        senderId: "70925429276",
        serverKey: "AAAAEIN8Khw:APA91bE21HwF98eP_JINlqJjz_ml95icnRC7XpQJhD0COYoM3A4gNJ8kaEn1-CWIPljXh3yLHI1NV6X2MfbQagRN-jvzSIRAT1wcUe06KZG5Rm8eUm5DF-viShOOEEDMtB9e-XsVxEzL"
    };

    constructor(
        public apiServiceProvider: ApiProvider
    ) {
        console.log('Hello FirebaseProvider Provider');
    }


    initializeFirebase() {
        firebase.initializeApp(FirebaseProvider.config);
    }


    private configureNotificaitons(registrationIds, title, message, data) {
        let notificationData = {
            "registration_ids": registrationIds,
            "notification": {
                "title": title,
                "body": message,
                "sound": 1,
                "badge": 1,
                'color': '#7cad31',
                'icon': 'notification_icon',
            },
            "data": {
                "data": data
            }
        };

        var headers = {
            'Content-Type': "application/json",
            'Authorization': "key=" + FirebaseProvider.config.serverKey
        }

        let url = "https://fcm.googleapis.com/fcm/send";

        return this.apiServiceProvider.callHttpPost(url, notificationData, headers);
    }

    public sendDeliveryRequestNotification(pushToken, senderName, senderId, message, requestId) {
        this.configureNotificaitons([pushToken], senderName, message,
            {
                type: "delivery_request_send",
                sender_id: senderId,
                driver_request_id: requestId
            });
    }

}
