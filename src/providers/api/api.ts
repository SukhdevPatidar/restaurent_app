import {Http, RequestOptions, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {Platform, LoadingController, Loading, ToastController} from 'ionic-angular';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
@Injectable()
export class ApiProvider {
    private loader: any;
    private toast: any;
    public isShowLoading = false;
    public navBar;
    public G_TRACKING_ID = "UA-129289281-1";

    constructor(
        public http: Http,
        public platform: Platform,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
    ) {
        console.log('Hello ApiProvider Provider');

        // this.platform.registerBackButtonAction((e) => {
        //   if(!this.isShowLoading) {
        //     if(this.nav.canGoBack()){
        //                 this.nav.pop();
        //     }
        //   }
        //
        //   return !this.isShowLoading;
        // });
    }

    static METHODS = {
        resCatList: "resCatList",
        resList: "resList",
        getCities: "getCities",
        resMenuList: "resMenuList",
        saveLocation: "saveLocation",
        sendLocation: "sendLocation",
        updateProfile: "updateProfile",
        getProfile: "getProfile",
        appSignin: "registerphone",
        getResUserPositions: "getResUserPositions",
        getMyLocations: "getMyLocations",
        checkMobileNo: "checkMobileNo",
        getDrivers: "getDrivers",
        sendDriverRequest: "sendDriverRequest",
    };

    isDevice() {
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            return false;
        } else {
            return true;
        }
    }

    getUser() {
        var user: any = localStorage.getItem("user_data");
        if (user) {
            user = JSON.parse(user);
            return user;
        }
        return null;
    }

    setUser(user) {
        if(user) {
            localStorage.setItem("user_data", JSON.stringify(user));
        }
    }

    showToast(msg, duration = 3000) {
        this.toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            position: 'top'
        });
        this.toast.present();
    }

    private callHttpGet(url, headers) {
        return new Promise((resolve, reject) => {
            let options = new RequestOptions({headers: headers});

            this.http.get(url, options)
            // .timeout(20000, new Error('Request Timeout'))
                .map(res => res.json())
                .subscribe(data => {
                        resolve(data);
                    },
                    error => {
                        reject(error);
                    });
        });
    }

    public callHttpPost(url, body, optionsObj) {
        return new Promise((resolve, reject) => {
            body = body ? body : {};

            var headers;
            if (optionsObj) {
                headers = new Headers(optionsObj);
            } else {
                headers = new Headers({'Content-Type': 'application/json'});
            }

            let options = new RequestOptions({headers: headers});

            this.http.post(url, body, options)
            // .timeout(20000, new Error("Request Timeout"))
                .map(res => res.json())
                .subscribe(data => {
                        console.log("POST Reponse " + JSON.stringify(data));
                        resolve(data);
                    },
                    (error) => {
                        reject(error);
                    }
                );
        });
    }

    public postRequest(methodName, postData = {}) {
        var ref = this;
        let url = this.getBaseApiUrl() + methodName;
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.callHttpPost(url, postData, {});
        // return this.callHttpPost(url, this.generateQueryString(postData), headers);
    }

    public getRequest(methodName, param = {}) {
        var ref = this;
        let url = this.getBaseApiUrl() + methodName + "?" + this.generateQueryString(param);
        let headers = {};
        return this.callHttpGet(url, headers);
    }

    public getBaseApiUrl() {
        if (this.isDevice()) {
            // return "http://neemuchmandirate.com/sdev/restaurents/";
            return "http://gommistaeconomy.it/velofood/";
        }
        return "http://localhost:8100/api/";
    }


    private generateQueryString(obj) {
        var str = [];
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    }


    public loading(msg = null) {
        this.loader = this.loadingCtrl.create({
            content: msg ? msg : ''
        });
        this.loader.present();
        this.isShowLoading = true;
    }

    public hideLoading() {
        if (this.loader) {
            this.loader.dismiss();
            this.loader = null;
            this.isShowLoading = false;
        }
    }


    public getImagePath(value, defaultImage) {
        if(!value) {
            return defaultImage;
        }
        return "http://gommistaeconomy.it/velofood/" + value.toLowerCase();
    }

}
