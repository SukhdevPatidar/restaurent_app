rm -rf app-release-signed.apk
ionic cordova build android --prod --release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore keystore/restaurent.keystore ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk restaurent
zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk app-release-signed.apk
